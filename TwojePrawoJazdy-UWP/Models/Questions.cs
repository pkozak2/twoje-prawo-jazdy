﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwojePrawoJazdy_UWP.Models
{
    public class Questions
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string First_answer { get; set; }
        public string Second_answer { get; set; }
        public string Third_answer { get; set; }
        public string Img { get; set; }
        public string Video { get; set; }
        public int Points { get; set; }
        public int Correct_answer { get; set; }
        public string Typ { get; set; }
        public string Comment { get; set; }
        public string Categories { get; set; }
        public int Is_premium { get; set; }

        public int TempSelectedAnswer { get; set; }
    }
}
