﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media;

namespace TwojePrawoJazdy_UWP.Models
{
    class ExamsListPageItem
    {
        public int TestId { get; set; }
        public int Number { get; set; }
        public string Result { get; set; }
        public string Category { get; set; }
        public int Points { get; set; }
        public DateTime SaveDate { get; set; }
        public SolidColorBrush ResultColor { get; set; }
    }
}
