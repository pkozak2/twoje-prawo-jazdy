﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwojePrawoJazdy_UWP.Models
{
    public class Product
    {
        public string Name;
        public string Price;

        public Product(string Name, string Price)
        {
            this.Name = Name;
            this.Price = Price;
        }
        public Product()
        {
            this.Name = "Error!";
            this.Price = "Error!";
        }
    }
}
