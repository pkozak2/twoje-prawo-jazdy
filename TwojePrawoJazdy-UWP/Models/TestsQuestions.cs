﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwojePrawoJazdy_UWP.Models
{
    public class TestsQuestions
    {
        public int QuestionId { get; set; }
        public int SelectedAnswer { get; set; }
    }
}
