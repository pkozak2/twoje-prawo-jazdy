﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwojePrawoJazdy_UWP.Models
{
    public class Favourites
    {
        public string UserId { get; set; }
        public int QuestionId { get; set; }
        public bool IsFavourited { get; set; }
    }
}
