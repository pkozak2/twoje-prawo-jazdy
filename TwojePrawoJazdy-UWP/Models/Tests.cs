﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwojePrawoJazdy_UWP.Models
{
     public class Tests
    {
        public int TestId { get; set; }
        public string UserId { get; set; }
        public string Category { get; set; }
        public int Points { get; set; }
        public bool IsPassed { get; set; }
        public DateTime SaveDate { get; set; }
        public ICollection<TestsQuestions> TestsQuestions { get; set; }

        public Tests()
        {
            TestsQuestions = new List<TestsQuestions>();
        }
    }
}
