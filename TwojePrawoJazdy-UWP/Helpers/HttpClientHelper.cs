﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using TwojePrawoJazdy_UWP.Services.SettingsServices;

namespace TwojePrawoJazdy_UWP.Helpers
{
    /// <summary>
    /// Klasa pomocnicza do wysyłania autoryzowanych zapytań do API
    /// </summary>
    class HttpClientHelper
    {
        /// <summary>
        /// Instancja klasy
        /// </summary>
        public static HttpClientHelper Instance { get; } = new HttpClientHelper();

        HttpClient client = new HttpClient();

        private string ApiUrl = SettingsService.Instance.ApiUrl;
        private string apiKey = SettingsService.Instance.ApiKey;

        /// <summary>
        /// Obiekt klasy pomocniczej do przekształcania obiektów na typ JSON
        /// </summary>
        private JsonHelper _jHelper = JsonHelper.Instance;

        /// <summary>
        /// Ustawia wymaganą autoryzację do API
        /// </summary>
        private void SetClientAuthorization()
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);
        }

        /// <summary>
        /// Pobiera element z API
        /// </summary>
        /// <param name="requestUrl">Dodatkowa część adresu API</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> GetFromUrl(string requestUrl)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                SetClientAuthorization();
                response = await client.GetAsync(ApiUrl + requestUrl);
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
            }
            
            return response;
        }
        /// <summary>
        /// Dodanie element do bazy API
        /// </summary>
        /// <typeparam name="T">Typ dodawanego obiektu</typeparam>
        /// <param name="postUrl">Dodatkowa część adresu API</param>
        /// <param name="postObject">Dodawany obiekt</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PostToUrl<T>(string postUrl, T postObject)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                var param = _jHelper.ModelToJson<T>(postObject);
                HttpContent content = new StringContent(param, Encoding.UTF8, "application/json");

                SetClientAuthorization();
                response = await client.PostAsync(ApiUrl + postUrl, content);
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
            }
            return response;
        }
        /// <summary>
        /// Edytuje element występujący pod adresem
        /// </summary>
        /// <typeparam name="T">Typ edytowanego obiektu</typeparam>
        /// <param name="putUrl">Dodatkowa część adresu API</param>
        /// <param name="putObject">Zaktualizaowany obiekt</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> PutToUrl<T>(string putUrl, T putObject)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                var param = _jHelper.ModelToJson<T>(putObject);
                HttpContent content = new StringContent(param, Encoding.UTF8, "application/json");

                SetClientAuthorization();
                response = await client.PutAsync(ApiUrl + putUrl, content);
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
            }
            return response;
        }

        /// <summary>
        /// Usuwa element pod wybranym adresem
        /// </summary>
        /// <param name="deleteUrl">Dodatkowa część adresu API</param>
        /// <returns></returns>
        public async Task<HttpResponseMessage> DeleteToUrl(string deleteUrl)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                SetClientAuthorization();
                response = await client.DeleteAsync(ApiUrl + deleteUrl);
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.ServiceUnavailable;
            }
            return response;
        }
    }
}
