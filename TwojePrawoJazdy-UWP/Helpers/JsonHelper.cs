﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace TwojePrawoJazdy_UWP.Helpers
{
    public class JsonHelper
    {
        public static JsonHelper Instance { get; } = new JsonHelper();

        public T JsonToObject<T>(string json)
        {
            return JsonConvert.DeserializeObject<T>(json);
        }
        public List<T> JsonToObjectList<T>(string json)
        {
            return JsonConvert.DeserializeObject<List<T>>(json);
        }
        public string ModelToJson<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
    }
}
