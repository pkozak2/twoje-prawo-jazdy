﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwojePrawoJazdy_UWP.Models;
using Windows.ApplicationModel.Store;
using Windows.UI.Popups;

namespace TwojePrawoJazdy.Services
{
    public class StoreService
    {
        public static StoreService Instance { get; }

        static StoreService()
        {
            // implement singleton pattern
            Instance = Instance ?? new StoreService();
        }

        public bool TestProduct(string productId)
        {
#if DEBUG
            LicenseInformation licenseInformation = CurrentAppSimulator.LicenseInformation;
#else
            LicenseInformation licenseInformation = CurrentApp.LicenseInformation;
#endif
            var productLicense = licenseInformation.ProductLicenses[productId];

            if (productLicense.IsActive)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<bool> BuyProductAsync(string productId)
        {
#if DEBUG
            LicenseInformation licenseInformation = CurrentAppSimulator.LicenseInformation;
#else
            LicenseInformation licenseInformation = CurrentApp.LicenseInformation;
#endif
            if (!licenseInformation.ProductLicenses[productId].IsActive)
            {
                Debug.WriteLine("Buying " + productId + "...");
                //rootPage.NotifyUser("Buying " + productName + "...", NotifyType.StatusMessage);
                try
                {
#if DEBUG
                    await CurrentAppSimulator.RequestProductPurchaseAsync(productId);
#else
                    await CurrentApp.RequestProductPurchaseAsync(productId);
#endif
                    if (licenseInformation.ProductLicenses[productId].IsActive)
                    {
                        Debug.WriteLine("Kupiono " + productId + "...");
                        return true;
                    }
                    else
                    {
                        Debug.WriteLine("Nie kupiono " + productId + "...");
                        return false;
                    }
                }
                catch (Exception)
                {
                    Debug.WriteLine("Nie kupiono " + productId + "...");
                    return false;
                }
            }
            else
            {
                Debug.WriteLine("Posiadany " + productId + "...");
                return true;
            }
        }

        public async Task<Product> getProductInformation(string productId)
        {
            try
            {
#if DEBUG
                ListingInformation listing = await CurrentAppSimulator.LoadListingInformationAsync();
#else
                ListingInformation listing = await CurrentApp.LoadListingInformationAsync();
#endif
                var product = listing.ProductListings[productId];
                return new Product(product.Name, product.FormattedPrice);
            }
            catch (Exception)
            {
                MessageDialog showDialog = new MessageDialog("Błąd połączenia ze sklepem. Sprawdź czy jesteś podłączony do sieci.");
                await showDialog.ShowAsync();
                Debug.WriteLine("LoadListingInformationAsync API call failed");
                return new Product();
            }
        }
        public async Task<bool> isProductOnSale(string productId)
        {
            try
            {
#if DEBUG
                ListingInformation listing = await CurrentAppSimulator.LoadListingInformationAsync();
#else
                ListingInformation listing = await CurrentApp.LoadListingInformationAsync();
#endif
                return listing.ProductListings[productId].IsOnSale;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
