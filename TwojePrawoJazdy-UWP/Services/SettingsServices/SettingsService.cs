using System;
using Template10.Common;
using Template10.Utils;
using Windows.UI.Xaml;

namespace TwojePrawoJazdy_UWP.Services.SettingsServices
{
    public class SettingsService
    {
        public static SettingsService Instance { get; } = new SettingsService();
        Template10.Services.SettingsService.ISettingsHelper _helper;
        private SettingsService()
        {
            _helper = new Template10.Services.SettingsService.SettingsHelper();
        }

        public ApplicationTheme AppTheme
        {
            get
            {
                var theme = ApplicationTheme.Light;
                var value = _helper.Read<string>(nameof(AppTheme), theme.ToString());
                return Enum.TryParse<ApplicationTheme>(value, out theme) ? theme : ApplicationTheme.Dark;
            }
            set
            {
                _helper.Write(nameof(AppTheme), value.ToString());
                (Window.Current.Content as FrameworkElement).RequestedTheme = value.ToElementTheme();
                Views.Shell.HamburgerMenu.RefreshStyles(value, true);
            }
        }

        public TimeSpan CacheMaxDuration
        {
            get { return _helper.Read<TimeSpan>(nameof(CacheMaxDuration), TimeSpan.FromDays(2)); }
            set
            {
                _helper.Write(nameof(CacheMaxDuration), value);
                BootStrapper.Current.CacheMaxDuration = value;
            }
        }

        public bool IsFullScreen
        {
            get { return _helper.Read<bool>(nameof(IsFullScreen), false); }
            set
            {
                _helper.Write(nameof(IsFullScreen), value);
                Views.Shell.HamburgerMenu.IsFullScreen = value;
            }
        }

        public string ApiUrl
        {
            get { return _helper.Read<string>(nameof(ApiUrl), ""); }
            set
            {
                _helper.Write(nameof(ApiUrl), value);

            }
        }

        public string Login
        {
            get { return _helper.Read<string>(nameof(Login), ""); }
            set
            {
                _helper.Write(nameof(Login), value);
                
            }
        }
        public string Password
        {
            get { return _helper.Read<string>(nameof(Password), ""); }
            set
            {
                _helper.Write(nameof(Password), value);
            }
        }
        public bool RememberLoginData
        {
            get { return _helper.Read<bool>(nameof(RememberLoginData), false); }
            set
            {
                _helper.Write(nameof(RememberLoginData), value);
            }
        }
        public string ApiKey
        {
            get { return _helper.Read<string>(nameof(ApiKey), ""); }
            set
            {
                _helper.Write(nameof(ApiKey), value);
            }
        }

        public string UserId
        {
            get { return _helper.Read<string>(nameof(UserId), ""); }
            set
            {
                _helper.Write(nameof(UserId), value);
            }
        }

        public bool IsPremium
        {
            get { return _helper.Read<bool>(nameof(IsPremium), false); }
            set
            {
                _helper.Write(nameof(IsPremium), value);
            }
        }

        public bool AutoSaveExams
        {
            get { return _helper.Read<bool>(nameof(AutoSaveExams), false); }
            set
            {
                _helper.Write(nameof(AutoSaveExams), value);
            }
        }
    }
}

