﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using System.Net.NetworkInformation;
using System.Net;

namespace TwojePrawoJazdy_UWP.Services.ApiServices
{
    class AuthorizationService
    {
        public static AuthorizationService Instance { get; } = new AuthorizationService();

        private SettingsService _settingService = SettingsService.Instance;

        private string ApiUrl = SettingsService.Instance.ApiUrl;

        private HttpClient client = new HttpClient();

        public async Task<HttpStatusCode> Login(string login, string password, bool remember)
        {

            var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>( "grant_type", "password" ),
                        new KeyValuePair<string, string>( "username", login ),
                        new KeyValuePair<string, string> ( "Password", password )
                    };
            var content = new FormUrlEncodedContent(pairs);

            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "x-www-form-urlencoded");

            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable()) {
                response = await client.PostAsync(ApiUrl + "/Token", content);
            }
            else {
                response.StatusCode = HttpStatusCode.ServiceUnavailable;
            }

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();
                var jObject = JObject.Parse(responseJson);
                _settingService.ApiKey = jObject.GetValue("access_token").ToString();
                _settingService.Login = login;
                _settingService.UserId = jObject.GetValue("UserId").ToString();
                if (remember)
                {

                    _settingService.Password = password;
                    _settingService.RememberLoginData = remember;
                    }
                else
                {
                    _settingService.Password = "";
                    _settingService.RememberLoginData = false;
                }
            }
            return response.StatusCode;
        }

        public async Task<HttpStatusCode> Register(string login, string email, string password, string confirmPassword)
        {
            var pairs = new List<KeyValuePair<string, string>>
                    {
                        new KeyValuePair<string, string>( "Login", login ),
                        new KeyValuePair<string, string>( "Email", email ),
                        new KeyValuePair<string, string> ( "Password", password ),
                        new KeyValuePair<string, string> ( "ConfirmPassword", confirmPassword ),
                    };
            var content = new FormUrlEncodedContent(pairs);

            client.DefaultRequestHeaders.TryAddWithoutValidation("Content-Type", "x-www-form-urlencoded");

            HttpResponseMessage response = new HttpResponseMessage();
            if (NetworkInterface.GetIsNetworkAvailable())
            {
                response = await client.PostAsync(ApiUrl + "/Account/Register", content);
            }
            else
            {
                response.StatusCode = HttpStatusCode.ServiceUnavailable;
            }

            return response.StatusCode;
        }

    }
}
