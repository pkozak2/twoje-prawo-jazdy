﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using TwojePrawoJazdy_UWP.Helpers;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.Services.DialogServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;

namespace TwojePrawoJazdy_UWP.Services.ApiServices
{
    class ApiService
    {
        public static ApiService Instance { get; } = new ApiService();

        private HttpClientHelper _httpClientHelper = HttpClientHelper.Instance;

        private JsonHelper _jHelper = JsonHelper.Instance;

        private DialogService _dialogService = DialogService.Instance;

        private SettingsService _settingsService = SettingsService.Instance;

        #region questions
        public async Task<Questions> GetQuestionById(int id)
        {
            Questions question = new Questions();

            HttpResponseMessage respnse = await _httpClientHelper.GetFromUrl("/Questions/" + id);

            if(respnse.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var responseJson = await respnse.Content.ReadAsStringAsync();

                question = _jHelper.JsonToObject<Questions>(respnse.ToString());
            }
            return question;
        }
        
        public async Task<List<Questions>> GetQuestionsForCategory(string category)
        {
            List<Questions> returnList = new List<Questions>();

            HttpResponseMessage response = await _httpClientHelper.GetFromUrl("/Questions/" + category + "/" + _settingsService.IsPremium);

            if(response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();

                returnList.AddRange(_jHelper.JsonToObjectList<Questions>(responseJson.ToString()));
            }
            else
            {
                _dialogService.NoInternetConnectionDialog();
            }

            return returnList;
        }

        public async Task<List<Questions>> GetQuestionsByIds(List<int> ids)
        {
            List<Questions> returnList = new List<Questions>();

            HttpResponseMessage response = await _httpClientHelper.PostToUrl<List<int>>("/Questions/Ids/", ids);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();

                returnList.AddRange(_jHelper.JsonToObjectList<Questions>(responseJson.ToString()));
            }
            else
            {
                _dialogService.NoInternetConnectionDialog();
            }

            return returnList;

        }
        #endregion

        #region exams
        public async Task<List<Questions>> GetExamQuestionsForCategory(string category)
        {
            List<Questions> returnList = new List<Questions>();

            HttpResponseMessage response = await _httpClientHelper.GetFromUrl("/Questions/" + category + "/" + _settingsService.IsPremium + "/exam");

            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();

                returnList.AddRange(_jHelper.JsonToObjectList<Questions>(responseJson.ToString()));
            }
            else
            {
                _dialogService.NoInternetConnectionDialog();
            }

            return returnList;
        }
        #endregion


        #region Favourites
        public async Task<List<Favourites>> GetUserFavourites(string userId)
        {
            List<Favourites> returnList = new List<Favourites>();

            HttpResponseMessage response = await _httpClientHelper.GetFromUrl("/Favourites/User/" + userId);
            if(response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();

                returnList.AddRange(_jHelper.JsonToObjectList<Favourites>(responseJson.ToString()));
            }
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
            }

            return returnList;
        }

        public async Task<bool> AddUserFavourite(Favourites item)
        {
            HttpResponseMessage response = await _httpClientHelper.PostToUrl<Favourites>("/Favourites", item);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                return true;
            }
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
                return false;
            }
            else {
                return false;
             }
        }
        public async Task<bool> DeleteUserFavourite(Favourites item)
        {
            HttpResponseMessage response = await _httpClientHelper.DeleteToUrl("/Favourites/" + item.UserId + "/" + item.QuestionId);
            string res = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
                return false;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Exams
        public async Task<List<Tests>> GetUserExams(string userId)
        {
            List<Tests> UserTests = new List<Tests>();
            HttpResponseMessage response = await _httpClientHelper.GetFromUrl("/Tests/user/" + userId);
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var responseJson = await response.Content.ReadAsStringAsync();

                UserTests.AddRange(_jHelper.JsonToObjectList<Tests>(responseJson.ToString()));
            }
            else if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
            }
            return UserTests;
        }

        public async Task<bool> SaveUserExam(Tests exam)
        {
            HttpResponseMessage response = await _httpClientHelper.PostToUrl<Tests>("/Tests", exam);
            if (response.StatusCode == HttpStatusCode.Created)
            {
                return true;
            }
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
                return false;
            }
            else
            {
                _dialogService.ShowMessage("Błąd przy zapisie egzaminu.");
                return false;
            }
        }

        public async Task<bool> DeleteUserExam(int examId)
        {
            
            HttpResponseMessage response = await _httpClientHelper.DeleteToUrl("/Tests/" + examId);
            var code = response.StatusCode;
            var res = response.Content.ReadAsStringAsync();
            if(response.StatusCode == HttpStatusCode.OK)
            {
                return true;
            }
            if(response.StatusCode == HttpStatusCode.ServiceUnavailable)
            {
                _dialogService.NoInternetConnectionDialog();
                return false;
            }
            else
            {
                _dialogService.ShowMessage("Błąd przy usuwaniu egzaminu. Spróbuj ponownie później.");
                return false;

            }
        }
        #endregion

    }
}
