﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Popups;

namespace TwojePrawoJazdy_UWP.Services.DialogServices
{
    class DialogService
    {
        public static DialogService Instance = new DialogService();

        public async void ShowDialog(string message)
        {
            MessageDialog showDialog = new MessageDialog(message);

            showDialog.Commands.Add(new UICommand("Yes") { Id = 0 });

            showDialog.Commands.Add(new UICommand("No") { Id = 1 });

            showDialog.DefaultCommandIndex = 0;

            showDialog.CancelCommandIndex = 1;

            await showDialog.ShowAsync();
        }

        public async void NoInternetConnectionDialog()
        {
            MessageDialog showDialog = new MessageDialog("Brak wymaganego połączenia z Internetem.");
            await showDialog.ShowAsync();
        }

        public async void ShowMessage(string message)
        {
            MessageDialog showDialog = new MessageDialog(message);
            await showDialog.ShowAsync();
        }
    }
}
