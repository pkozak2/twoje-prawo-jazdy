using Windows.UI.Xaml;
using System.Threading.Tasks;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.ApplicationModel.Activation;
using Template10.Controls;
using Template10.Common;
using System;
using System.Linq;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Controls;
using Windows.Storage;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Store;
using TwojePrawoJazdy.Services;
using Windows.Foundation.Metadata;
using Windows.UI.ViewManagement;

namespace TwojePrawoJazdy_UWP
{
    /// Documentation on APIs used in this page:
    /// https://github.com/Windows-XAML/Template10/wiki

    [Bindable]
    sealed partial class App : BootStrapper
    {
        public static StoreService storeService = new StoreService();
        public App()
        {
            InitializeComponent();
            SplashFactory = (e) => new Views.Splash(e);

            #region app settings

            // some settings must be set in app.constructor
            var settings = SettingsService.Instance;
            RequestedTheme = settings.AppTheme;
            AutoSuspendAllFrames = true;
            AutoRestoreAfterTerminated = true;
            AutoExtendExecutionSession = true;

            settings.ApiUrl = "http://testyprawojazdy.pkozak.aspnet.pl";
            //settings.ApiUrl = "http://localhost:36893";

            checkLicenseAndConfigureSimulator("in-app-purchase.xml");
            #endregion
        }

        public override UIElement CreateRootElement(IActivatedEventArgs e)
        {
            var service = NavigationServiceFactory(BackButton.Attach, ExistingContent.Exclude);
            return new ModalDialog
            {
                DisableBackButtonWhenModal = true,
                Content = new Views.Shell(service),
                ModalContent = new Views.Busy(),
            };
        }

        public override async Task OnStartAsync(StartKind startKind, IActivatedEventArgs args)
        {

            //Mobile customization
            if (ApiInformation.IsTypePresent("Windows.UI.ViewManagement.StatusBar"))
            {

                var statusBar = StatusBar.GetForCurrentView();
                await statusBar.HideAsync();
            }

            // TODO: add your long-running task here
            await NavigationService.NavigateAsync(typeof(Views.MainPage));
        }

        private async void checkLicenseAndConfigureSimulator(string filename)
        {
#if DEBUG
            StorageFile proxyFile = await Package.Current.InstalledLocation.GetFileAsync("IAPTest\\" + filename);
            await CurrentAppSimulator.ReloadSimulatorAsync(proxyFile);
#endif
            if (storeService.TestProduct("premium"))
            {
                SettingsService.Instance.IsPremium = true;
            }
            else
            {
                SettingsService.Instance.IsPremium = false;
            }
        }

    }
}

