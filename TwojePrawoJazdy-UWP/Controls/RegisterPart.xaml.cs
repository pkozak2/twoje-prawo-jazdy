﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.DialogServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace TwojePrawoJazdy_UWP.Controls
{

    public sealed partial class RegisterPart : UserControl
    {
        public RegisterPart()
        {
            this.InitializeComponent();
        }

        public event EventHandler HideRequested;

        string _LoginValue = "";
        public string LoginValue { get { return _LoginValue; } set { _LoginValue = value; } }
        string _EmailValue = "";
        public string EmailValue { get { return _EmailValue; } set { _EmailValue = value; } }
        string _PasswordValue = "";
        public string PasswordValue { get { return _PasswordValue; } set { _PasswordValue = value; } }
        string _ConfirmPasswordValue = "";
        public string ConfirmPasswordValue { get { return _ConfirmPasswordValue; } set { _ConfirmPasswordValue = value; } }


        public async void DoRegister(object sender, RoutedEventArgs e)
        {
            if (!IsFormCompleted())
            {
                return;
            }

            StatusValue.Text = "Trwa rejestracja...";
            ProgressRing.IsActive = true;
            try
            {
                HttpStatusCode result = await AuthorizationService.Instance.Register(LoginValue, EmailValue, PasswordValue, ConfirmPasswordValue);

                switch (result)
                {
                    case HttpStatusCode.OK:
                        ProgressRing.IsActive = false;
                        HideRequested?.Invoke(this, EventArgs.Empty);
                        DialogService.Instance.ShowMessage("Zarejestrowano. Proszę się zalogować.");
                        break;
                    case HttpStatusCode.ServiceUnavailable:
                        StatusValue.Text = "";
                        DialogService.Instance.NoInternetConnectionDialog();
                        ProgressRing.IsActive = false;
                        break;
                    case HttpStatusCode.InternalServerError:
                        ProgressRing.IsActive = false;
                        StatusValue.Text = "Użytkownik już istnieje.";
                        break;
                    case HttpStatusCode.BadRequest:
                        ProgressRing.IsActive = false;
                        StatusValue.Text = "Niepoprawne dane";
                        break;
                    default:
                        ProgressRing.IsActive = false;
                        StatusValue.Text = "Błąd wewnętzny.";
                        break;
                }
            }
            catch (Exception)
            {
                ProgressRing.IsActive = false;
                StatusValue.Text = "Błąd wewnętrzny.";
            }
        }

        private bool IsFormCompleted()
        {
            if (string.IsNullOrWhiteSpace(LoginValue))
            {
                StatusValue.Text = "Login jest wymagany.";
                return false;
            }
            if (string.IsNullOrWhiteSpace(PasswordValue) || string.IsNullOrWhiteSpace(ConfirmPasswordValue))
            {
                StatusValue.Text = "Hasło jest wymagane.";
                return false;
            }
            if (PasswordValue != ConfirmPasswordValue)
            {
                StatusValue.Text = "Podane hasła różnią się.";
                return false;
            }
            if (string.IsNullOrWhiteSpace(EmailValue) || !Regex.IsMatch(EmailValue, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"))
            {
                StatusValue.Text = "Niepoprawny adres e-mail.";
                return false;
            }
            return true;
        }
        
        private void CloseClicked(object sender, RoutedEventArgs e)
        {
            HideRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}
