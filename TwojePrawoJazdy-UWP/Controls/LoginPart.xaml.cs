﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.DialogServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace TwojePrawoJazdy_UWP.Controls
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPart : UserControl
    {
        public LoginPart()
        {
            this.InitializeComponent();
        }

        public event EventHandler RegisterRequested;
        public event EventHandler LoggedIn;

        string _LoginValue = SettingsService.Instance.Login;
        public string LoginValue { get { return _LoginValue; } set { _LoginValue = value; } }
        string _PasswordValue = SettingsService.Instance.Password;
        public string PasswordValue { get { return _PasswordValue; } set { _PasswordValue = value; } } 
        bool _RememberDataValue = SettingsService.Instance.RememberLoginData;
        public bool RememberDataValue { get { return _RememberDataValue; } set { _RememberDataValue = value; } }


        public async void DoLogin(object sender, RoutedEventArgs e)
        {
            FieldsDisabler();

            StatusValue.Text = "Logowanie...";
            try
            {
                HttpStatusCode result = await AuthorizationService.Instance.Login(LoginValue, PasswordValue, RememberDataValue);

                switch (result)
                {
                    case HttpStatusCode.OK:
                        LoggedIn?.Invoke(this, EventArgs.Empty);
                        break;
                    case HttpStatusCode.ServiceUnavailable:
                        StatusValue.Text = "";
                        DialogService.Instance.NoInternetConnectionDialog();
                        break;
                    case HttpStatusCode.BadRequest:
                        StatusValue.Text = "Niepoprawne dane logowania.";
                        break;
                    default:
                        StatusValue.Text = "Błąd wewnętzny.";
                        break;
                }
            }
            catch (Exception)
            {
                ProgressRing.IsActive = false;
                StatusValue.Text = "Błąd wewnętrzny.";
            }
            FieldsDisabler();
        }

        private void FieldsDisabler()
        {
            ProgressRing.IsActive = !ProgressRing.IsActive;
            Login.IsEnabled = !Login.IsEnabled;
            Password.IsEnabled = !Password.IsEnabled;
            RememberData.IsEnabled = !RememberData.IsEnabled;
            LoginSubmit.IsEnabled = !LoginSubmit.IsEnabled;
            Register.IsEnabled = !Register.IsEnabled;
        }

        private void RegisterClicked(object sender, RoutedEventArgs e)
        {
            RegisterRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}
