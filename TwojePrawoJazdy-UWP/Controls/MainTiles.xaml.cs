﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using Template10.Mvvm;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at https://go.microsoft.com/fwlink/?LinkId=234236

namespace TwojePrawoJazdy_UWP.Controls
{
    public sealed partial class MainTiles : UserControl
    {
        public MainTiles()
        {
            this.InitializeComponent();
        }

        public static readonly DependencyProperty CategoryProperty =
            DependencyProperty.Register("CategoryProperty", typeof(string), typeof(MainTiles), new PropertyMetadata(default(string)));

        public static readonly DependencyProperty ImageUriProperty =
            DependencyProperty.Register("ImageUriProperty", typeof(Uri), typeof(MainTiles), new PropertyMetadata(default(Uri)));

        public static readonly DependencyProperty ColorProperty =
            DependencyProperty.Register("ColorProperty", typeof(SolidColorBrush), typeof(MainTiles), new PropertyMetadata(default(SolidColorBrush)));

        public static readonly DependencyProperty LearnCommandProperty =
            DependencyProperty.Register("LearnCommand", typeof(ICommand), typeof(MainTiles), new PropertyMetadata(default(ICommand)));

        public static readonly DependencyProperty ExamCommandProperty =
            DependencyProperty.Register("ExamCommand", typeof(ICommand), typeof(MainTiles), new PropertyMetadata(default(ICommand)));


        public string Category
        {
            get { return (string)GetValue(CategoryProperty); }
            set { SetValue(CategoryProperty, value); }
        }

        public string ImageUri
        {
            get { return (string)GetValue(ImageUriProperty); }
            set { SetValue(ImageUriProperty, value); }
        }

        public string Color
        {
            get { return (string)GetValue(ColorProperty); }
            set { SetValue(ColorProperty, value); }
        }

        public ICommand LearnCommand
        {
            get { return base.GetValue(LearnCommandProperty) as ICommand; }
            set { base.SetValue(LearnCommandProperty, value); }
        }

        public ICommand ExamCommand
        {
            get { return base.GetValue(ExamCommandProperty) as ICommand; }
            set { base.SetValue(ExamCommandProperty, value); }
        }

    }
}
