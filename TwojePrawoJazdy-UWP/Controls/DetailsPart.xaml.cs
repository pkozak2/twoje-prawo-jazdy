﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;


namespace TwojePrawoJazdy_UWP.Controls
{
    public sealed partial class DetailsPart
    {

        public DetailsPart()
        {
            this.InitializeComponent();
        }

        public event EventHandler HideRequested;

        public static readonly DependencyProperty DetailsProperty =
            DependencyProperty.Register("DetailsValue", typeof(string), typeof(DetailsPart), new PropertyMetadata(default(string)));

        public string DetailsValue
        {
            get { return (string)GetValue(DetailsProperty); }
            set { SetValue(DetailsProperty, value); }
        }

        private void CloseClicked(object sender, RoutedEventArgs e)
        {
            HideRequested?.Invoke(this, EventArgs.Empty);
        }
    }
}
