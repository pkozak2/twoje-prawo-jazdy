using System.ComponentModel;
using System.Linq;
using System;
using Template10.Common;
using Template10.Controls;
using Template10.Services.NavigationService;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Template10.Mvvm;

namespace TwojePrawoJazdy_UWP.Views
{
    public sealed partial class Shell : Page
    {
        public static Shell Instance { get; set; }
        public static HamburgerMenu HamburgerMenu => Instance.MyHamburgerMenu;
        Services.SettingsServices.SettingsService _settings;

        public Shell()
        {
            Instance = this;
            InitializeComponent();
            _settings = Services.SettingsServices.SettingsService.Instance;
            LoginModal.IsModal = true;
        }

        public Shell(INavigationService navigationService) : this()
        {
            SetNavigationService(navigationService);
        }

        public void SetNavigationService(INavigationService navigationService)
        {
            MyHamburgerMenu.NavigationService = navigationService;
            HamburgerMenu.RefreshStyles(_settings.AppTheme, true);
            HamburgerMenu.IsFullScreen = _settings.IsFullScreen;
            HamburgerMenu.HamburgerButtonVisibility = Visibility.Visible;
        }

        #region LoginRegisterModals

        private void LoginLoggedIn(object sender, EventArgs e)
        {
            LoginModal.IsModal = false;
            LoginText.Text = _settings.Login;
        }

        private void RegisterHide(object sender, EventArgs e)
        {
            RegisterModal.IsModal = false;
            LoginModal.IsModal = true;
        }

        private void Register(object sender, EventArgs e)
        {
            RegisterModal.IsModal = true;
            LoginModal.IsModal = false;
        }
        #endregion
    }
}

