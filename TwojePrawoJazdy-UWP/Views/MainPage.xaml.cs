using System;
using TwojePrawoJazdy_UWP.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System.Collections.ObjectModel;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Models;
using System.Collections.Generic;

namespace TwojePrawoJazdy_UWP.Views
{
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            InitializeComponent();
            NavigationCacheMode = Windows.UI.Xaml.Navigation.NavigationCacheMode.Enabled;
        }
    }
}
