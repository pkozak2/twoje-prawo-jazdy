﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.ViewModels;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.System.Profile;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace TwojePrawoJazdy_UWP.Views
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
public sealed partial class ExamResultPage : Page
    {
        int questionNumber;
        ExamResultPageViewModel vm;

        public ExamResultPage()
        {  
            this.InitializeComponent();
            NavigationCacheMode = NavigationCacheMode.Disabled;

            questionNumber = 1;
            vm = this.DataContext as ExamResultPageViewModel;
            vm.AddButtonEvent += (sender, question) =>
            {
                AddButton(question);
            };
        }
        
        public void AddButton(Questions question)
        {
            Button btn = new Button();

            btn.Name = "Btn-" + questionNumber.ToString();
            btn.Height = 50;
            btn.Width = 50;
            btn.Content = questionNumber.ToString();
            btn.Tag = questionNumber - 1;
            btn.Margin = new Thickness(5, 5, 5, 5);
            btn.Style = Resources["MyButtonStyle"] as Style;
            btn.Foreground = new SolidColorBrush(Colors.White);
            if (question.Correct_answer == question.TempSelectedAnswer)
            {
                btn.Background = new SolidColorBrush(Colors.DarkGreen);
            }
            else
            {
                btn.Background = new SolidColorBrush(Colors.DarkRed);
            }

            btn.Click += Btn_Click;

            myGridView.Children.Add(btn);

            questionNumber++;
        }

        private void Btn_Click(object sender, RoutedEventArgs e)
        {
            var question = (Button)sender;
            int id = Int32.Parse(question.Tag.ToString());
            if (vm.ShowDetailsCommand.CanExecute(id)){
                vm.ShowDetailsCommand.Execute(id);
            }
        }
    }
}
