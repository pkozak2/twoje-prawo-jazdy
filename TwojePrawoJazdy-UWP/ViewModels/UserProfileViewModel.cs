﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    class UserProfileViewModel : ViewModelBase
    {
        private string _LoginValue = "Profil";
        public string LoginValue
        {
            get
            {
                return _LoginValue;
            }
            set
            {
                _LoginValue = value; base.RaisePropertyChanged();
            }
        }
    }
}
