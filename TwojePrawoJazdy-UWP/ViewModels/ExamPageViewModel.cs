﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using Template10.Services.NavigationService;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    class ExamPageViewModel : ViewModelBase
    {
        #region Definitions
        DispatcherTimer timer = new DispatcherTimer();
        int time = 20;
        int etap = 0;
        public bool videoPlayed = false;
        int CurrentAnswer = 0;

        private string _Category = "abcdt";
        public string Category { get { return _Category; } set { Set(ref _Category, value); } }

        private Visibility _DataLoaded = Visibility.Collapsed;
        public Visibility DataLoaded { get { return _DataLoaded; } set { Set(ref _DataLoaded, value); } }

        private Visibility _VideoVisibility = Visibility.Collapsed;
        public Visibility VideoVisibility { get { return _VideoVisibility; } set { Set(ref _VideoVisibility, value); } }

        private Visibility _ImageVisibility = Visibility.Collapsed;
        public Visibility ImageVisibility { get { return _ImageVisibility; } set { Set(ref _ImageVisibility, value); } }

        private int _CurrentQuestionNumberView = 0;
        public int CurrentQuestionNumberView
        {
            get { return _CurrentQuestionNumberView; }
            set { Set(ref _CurrentQuestionNumberView, value); }
        }

        private int _LoadedQuestionsNumber = 0;
        public int LoadedQuestionsNumber
        {
            get { return _LoadedQuestionsNumber; }
            set { Set(ref _LoadedQuestionsNumber, value); }
        }

        private int _CurrentQuestionPoints = 0;
        public int CurrentQuestionPoints
        {
            get { return _CurrentQuestionPoints; }
            set { Set(ref _CurrentQuestionPoints, value); }
        }

        private int _TimerTicker = 20;
        public int TimerTicker
        {
            get { return _TimerTicker; }
            set { Set(ref _TimerTicker, value); }
        }

        private Uri _VideoElementSource = null;
        public Uri VideoElementSource
        {
            get { return _VideoElementSource; }
            set { Set(ref _VideoElementSource, value); }
        }

        private BitmapImage _ImageElementSource = null;
        public BitmapImage ImageElementSource
        {
            get { return _ImageElementSource; }
            set { Set(ref _ImageElementSource, value); }
        }

        private string _QuestionText = "Question text";
        public string QuestionText
        {
            get { return _QuestionText; }
            set { Set(ref _QuestionText, value); }
        }

        private bool _FirstAnswerChecked = false;
        public bool FirstAnswerChecked
        {
            get { return _FirstAnswerChecked; }
            set { Set(ref _FirstAnswerChecked, value); base.RaisePropertyChanged(); }
        }
        private bool _SecondAnswerChecked = false;
        public bool SecondAnswerChecked
        {
            get { return _SecondAnswerChecked; }
            set { Set(ref _SecondAnswerChecked, value); base.RaisePropertyChanged(); }
        }
        private bool _ThirdAnswerChecked = false;
        public bool ThirdAnswerChecked
        {
            get { return _ThirdAnswerChecked; }
            set { Set(ref _ThirdAnswerChecked, value); base.RaisePropertyChanged(); }
        }        

        private string _TimerText = "Czas na zapoznanie się z pytaniem";
        public string TimerText
        {
            get { return _TimerText; }
            set { Set(ref _TimerText, value); }
        }

        private string _FirstAnswerText = "First Option";
        public string FirstAnswerText
        {
            get { return _FirstAnswerText; }
            set { Set(ref _FirstAnswerText, value); }
        }
        private string _SecondAnswerText = "Second Option";
        public string SecondAnswerText
        {
            get { return _SecondAnswerText; }
            set { Set(ref _SecondAnswerText, value); }
        }
        private string _ThirdAnswerText = "Third Option";
        public string ThirdAnswerText
        {
            get { return _ThirdAnswerText; }
            set { Set(ref _ThirdAnswerText, value); }
        }

        private string _QuestionComment = "Question Comment";
        public string QuestionComment
        {
            get { return _QuestionComment; }
            set { Set(ref _QuestionComment, value); }
        }

        private SolidColorBrush _FirstAnswerBackground = null;
        public SolidColorBrush FirstAnswerBackground
        {
            get { return _FirstAnswerBackground; }
            set { Set(ref _FirstAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _SecondAnswerBackground = null;
        public SolidColorBrush SecondAnswerBackground
        {
            get { return _SecondAnswerBackground; }
            set { Set(ref _SecondAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _ThirdAnswerBackground = null;
        public SolidColorBrush ThirdAnswerBackground
        {
            get { return _ThirdAnswerBackground; }
            set { Set(ref _ThirdAnswerBackground, value); base.RaisePropertyChanged(); }
        }

        private Visibility _ThirdAnswerVisibility = Visibility.Visible;
        public Visibility ThirdAnswerVisibility
        {
            get { return _ThirdAnswerVisibility; }
            set { Set(ref _ThirdAnswerVisibility, value); }
        }

        private Visibility _PlayButtonVisibility = Visibility.Collapsed;
        public Visibility PlayButtonVisibility
        {
            get { return _PlayButtonVisibility; }
            set { Set(ref _PlayButtonVisibility, value); }
        }

        DelegateCommand _QuestionButtonNextLoadCommand;
        public DelegateCommand QuestionButtonNextLoadCommand => _QuestionButtonNextLoadCommand ?? (_QuestionButtonNextLoadCommand = new DelegateCommand(QuestionButtonNextLoader));

        DelegateCommand<string> _AnswerButtonCheckedCommand;
        public DelegateCommand<string> AnswerButtonCheckedCommand => _AnswerButtonCheckedCommand ?? (_AnswerButtonCheckedCommand = new DelegateCommand<string>(AnswerButtonChecked));

        DelegateCommand _PlayButtonCommand;
        public DelegateCommand PlayButtonCommand => _PlayButtonCommand ?? (_PlayButtonCommand = new DelegateCommand(PlayButtonClicked));

        DelegateCommand _LastButtonCommand;
        public DelegateCommand LastButtonCommand => _LastButtonCommand ?? (_LastButtonCommand = new DelegateCommand(LastButtonClicked));

        public event EventHandler PlayRequested;

        int CurrentQuestionId = 0;
        int CurrentQuestionNumber = 0;
        List<Questions> questionsList = new List<Questions>();
        #endregion

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            Views.Busy.SetBusy(true, "Proszę czekać...");
            Category = (suspensionState.ContainsKey(nameof(Category))) ? suspensionState[nameof(Category)]?.ToString() : parameter?.ToString();

            Debug.WriteLine("Uruchomiono z parametrem: " + Category);

            questionsList = await ApiService.Instance.GetExamQuestionsForCategory(Category);

            Debug.WriteLine("Załadowano pytań: " + questionsList.Count());

            LoadedQuestionsNumber = questionsList.Count();

            StartExam();

            DataLoaded = Visibility.Visible;
            Views.Busy.SetBusy(false);
        }

        public override Task OnNavigatingFromAsync(NavigatingEventArgs args)
        {
            timer.Stop();
            return base.OnNavigatingFromAsync(args);
        }

        private void LoadImageVideo(int id)
        {
            if (string.IsNullOrEmpty(questionsList[id].Img))
            {
                ImageVisibility = Visibility.Collapsed;
                VideoVisibility = Visibility.Visible;
                VideoElementSource = new Uri("ms-appx:///Assets/Videos/" + questionsList[id].Video);
                PlayButtonVisibility = Visibility.Visible;
            }
            else
            {
                ImageVisibility = Visibility.Visible;
                VideoVisibility = Visibility.Collapsed;
                ImageElementSource = new BitmapImage(new Uri("ms-appx:///Assets/Images/" + questionsList[id].Img));
                videoPlayed = true;
                PlayButtonVisibility = Visibility.Collapsed;
            }
        }

        private void LoadPossibleAnswers(int id)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            FirstAnswerChecked = false;
            SecondAnswerChecked = false;
            ThirdAnswerChecked = false;

            if (string.IsNullOrEmpty(questionsList[id].First_answer) && string.IsNullOrEmpty(questionsList[id].Second_answer) &&
                string.IsNullOrEmpty(questionsList[id].Third_answer))
            {
                ThirdAnswerVisibility = Visibility.Collapsed;
                FirstAnswerText = "TAK";
                SecondAnswerText = "NIE";
            }
            else
            {
                ThirdAnswerVisibility = Visibility.Visible;
                FirstAnswerText = questionsList[id].First_answer;
                SecondAnswerText = questionsList[id].Second_answer;
                ThirdAnswerText = questionsList[id].Third_answer;
            }
        }

        private void LoadQuestion(int id)
        {
            CurrentAnswer = 0;
            QuestionText = questionsList[id].Question;
            CurrentQuestionNumberView = (CurrentQuestionNumber + 1);
            CurrentQuestionPoints = questionsList[id].Points;
            CurrentQuestionId = questionsList[id].Id;
            questionsList[id].Categories = Category;
            LoadImageVideo(id);
            LoadPossibleAnswers(id);
        }

        public void StartExam()
        {
            LoadQuestion(CurrentQuestionNumber);

            timer.Interval = TimeSpan.FromSeconds(1);
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Timer_Tick(object sender, object e)
        {
            TimerTicker = time;

            if (etap == 0)
            {
                if (time > 0)
                {
                    TimerText = "Czas na zapoznanie się z pytaniem";
                    TimerTicker = (--time);
                }
                else if (videoPlayed == false)
                {
                    etap = 1;
                    // czas na video
                    this.PlayRequested(this, EventArgs.Empty);
                    PlayButtonVisibility = Visibility.Collapsed;

                }
                else if (videoPlayed == true)
                {
                    etap = 2;
                    time = 15; // czas na odpowiedź
                }

            }
            if (etap == 1)
            {
                TimerText = "Czas na zapoznanie się z materiałem";
                TimerTicker = (++time);

                if (videoPlayed == true)
                {
                    etap = 2;
                    time = 15; // czas na odpowiedź
                }
            }
            if (etap == 2)
            {
                if (time > 0)
                {
                    TimerText = "Czas na odpowiedź";
                    TimerTicker = (--time);
                }
                else
                {
                    QuestionButtonNextLoader();
                }
            }
        }

        private async void QuestionButtonNextLoader()
        {
            // reset timer elements
            etap = 0;
            time = 20;
            videoPlayed = false;
            // reset timer elements

            CurrentQuestionNumber = CurrentQuestionNumber + 1;

            if (CurrentQuestionNumber + 1 <= questionsList.Count())
            {
                questionsList[CurrentQuestionNumber - 1].TempSelectedAnswer = CurrentAnswer;

                LoadQuestion(CurrentQuestionNumber);
            }
            if (CurrentQuestionNumber == questionsList.Count())
            {
                timer.Stop();
                questionsList[CurrentQuestionNumber - 1].TempSelectedAnswer = CurrentAnswer;
                var state = Template10.Common.BootStrapper.Current.SessionState;
                state.Add("Questions", questionsList);

                if (NavigationService.CanGoBack)
                { // Disable back to ExamPage and get new question.
                    NavigationService.GoBack();
                }

                NavigationService.Navigate(typeof(Views.ExamResultPage), "Questions");
            }
        }

        private void AnswerButtonChecked(string sender)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            CurrentAnswer = Int32.Parse(sender);
            switch (CurrentAnswer)
            {
                case 1:
                    FirstAnswerBackground = new SolidColorBrush(Colors.Green);
                    FirstAnswerChecked = true;
                    break;
                case 2:
                    SecondAnswerBackground = new SolidColorBrush(Colors.Green);
                    SecondAnswerChecked = true;
                    break;
                case 3:
                    ThirdAnswerBackground = new SolidColorBrush(Colors.Green);
                    ThirdAnswerChecked = true;
                    break;
            }
        }

        private void PlayButtonClicked()
        {
            //Clear timer to go to next counting step
            time = 0;
        }


        private void LastButtonClicked()
        {
            CurrentQuestionNumber = questionsList.Count - 1;
            LoadQuestion(questionsList.Count-1);
            
        }
    }
}
