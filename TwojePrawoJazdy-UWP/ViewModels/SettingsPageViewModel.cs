using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Template10.Mvvm;
using Template10.Services.SettingsService;
using TwojePrawoJazdy_UWP.Models;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    public class SettingsPageViewModel : ViewModelBase
    {
        public SettingsPartViewModel SettingsPartViewModel { get; } = new SettingsPartViewModel();
        public PremiumPartViewModel PremiumPartViewModel { get; } = new PremiumPartViewModel();
        public AboutPartViewModel AboutPartViewModel { get; } = new AboutPartViewModel();
    }

    public class SettingsPartViewModel : ViewModelBase
    {
        Services.SettingsServices.SettingsService _settings;

        public SettingsPartViewModel()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                // designtime
            }
            else
            {
                _settings = Services.SettingsServices.SettingsService.Instance;
            }
        }

        public bool UseLightThemeButton
        {
            get { return _settings.AppTheme.Equals(ApplicationTheme.Light); }
            set { _settings.AppTheme = value ? ApplicationTheme.Light : ApplicationTheme.Dark; base.RaisePropertyChanged(); }
        }

        public bool AutoSaveExamsButton
        {
            get { return _settings.AutoSaveExams; }
            set { _settings.AutoSaveExams = value ? true : false; base.RaisePropertyChanged(); }
        }
    }

    public class PremiumPartViewModel : ViewModelBase
    {
        Services.SettingsServices.SettingsService _settings;

        DelegateCommand _ActivatePremiumCommand;
        public DelegateCommand ActivatePremiumCommand => _ActivatePremiumCommand ?? (_ActivatePremiumCommand = new DelegateCommand(ActivatePremiumAsync));

        DelegateCommand _TestPremiumCommand;
        public DelegateCommand TestPremiumCommand => _TestPremiumCommand ?? (_TestPremiumCommand = new DelegateCommand(TestPremiumAsync));

        string productId = "premium";

        string _ProductName = "Produkt";
        public string ProductName
        {
            get { return _ProductName; }
            set { Set(ref _ProductName, value); }
        }

        string _ProductPrice = "0,00";
        public string ProductPrice
        {
            get { return _ProductPrice; }
            set { Set(ref _ProductPrice, value); }
        }

        public PremiumPartViewModel()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled)
            {
                // designtime
            }
            else
            {
                _settings = Services.SettingsServices.SettingsService.Instance;
            }

            LoadData();
        }

        private async void LoadData()
        {
            Product premium = await App.storeService.getProductInformation("premium");

            ProductName = premium.Name;
            ProductPrice = premium.Price;
        }

        public string ActivatePremiumHeader
        {
            get
            {
                return "Mo�esz odblokowa� wszystkie pytania, kt�re mog� pojawi� si� na Twoim egzaminie na Prawo Jazdy. By aktywowa� mo�liwo�� korzystania z pe�nej bazy pyta� wci�nij poni�szy przycisk.";
            }
        }
        public string ActivatePremiumButton
        {
            get
            {
                return "Aktywuj dost�p do pyta�";
            }
        }

        public string TestProductHeader
        {
            get
            {
                return "Je�li jeste� pewien, �e aktywowa�e� w przesz�o�ci dost�p do pe�nej bazy pyta�, a aplikacja uznaje, �e nie masz do nich dost�pu, mo�esz sprawdzi� czy posiadasz dost�p klikaj�c poni�ej.";
            }
        }
        public string TestProductButton
        {
            get
            {
                return "Sprawd� dost�p";
            }
        }


        private async void ActivatePremiumAsync()
        {
            if (!App.storeService.TestProduct(productId))
            {
                Debug.WriteLine("Buying " + ProductName + "...");
                try
                {
                    await App.storeService.BuyProductAsync(productId);
                    if (App.storeService.TestProduct(productId))
                    {
                        MessageDialog showDialog = new MessageDialog("Aktywacja udana! Posiadasz dost�p do pe�nej bazy pyta�.");
                        await showDialog.ShowAsync();
                        Debug.WriteLine("You bought " + ProductName + ".");

                        _settings.IsPremium = true;
                    }
                    else
                    {
                        MessageDialog showDialog = new MessageDialog("Niestety nie uda�o si� aktywowa� produktu. Spr�buj ponownie.");
                        await showDialog.ShowAsync();
                        Debug.WriteLine(ProductName + " was not purchased.");
                        _settings.IsPremium = false;
                    }
                }
                catch (Exception)
                {
                    MessageDialog showDialog = new MessageDialog("Niestety nie uda�o si� aktywowa� produktu. Spr�buj ponownie.");
                    await showDialog.ShowAsync();
                    Debug.WriteLine("Unable to buy " + ProductName + ".");
                    _settings.IsPremium = false;
                }
            }
            else
            {
                MessageDialog showDialog = new MessageDialog("Ju� posiadasz us�ug�!");
                await showDialog.ShowAsync();
                Debug.WriteLine("You already own " + ProductName + ".");
                _settings.IsPremium = true;
            }
        }

        private async void TestPremiumAsync()
        {
            if (App.storeService.TestProduct("premium"))
            {
                MessageDialog showDialog = new MessageDialog("Posiadasz dost�p do pe�nej bazy pyta�.");
                await showDialog.ShowAsync();
                _settings.IsPremium = true;
            }
            else
            {
                MessageDialog showDialog = new MessageDialog("Niestety nie posiadasz dost�pu do pe�nej bazy pyta�.");
                await showDialog.ShowAsync();
                _settings.IsPremium = false;
            }
        }


    }

    public class AboutPartViewModel : ViewModelBase
    {
        public Uri Logo => Windows.ApplicationModel.Package.Current.Logo;

        public string DisplayName => Windows.ApplicationModel.Package.Current.DisplayName;

        public string Publisher => Windows.ApplicationModel.Package.Current.PublisherDisplayName;

        public string Version
        {
            get
            {
                var v = Windows.ApplicationModel.Package.Current.Id.Version;
                return $"{v.Major}.{v.Minor}.{v.Build}.{v.Revision}";
            }
        }

        public Uri Api => new Uri("http://testyprawojazdy.pkozak.aspnet.pl/");
    }
}

