﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    class LearnPageViewModel : ViewModelBase
    {
    #region definitions
        private string _Category = "abcdt";
        public string Category { get { return _Category; } set { Set(ref _Category, value); } }

        private Visibility _DataLoaded = Visibility.Collapsed;
        public Visibility DataLoaded { get { return _DataLoaded; } set { Set(ref _DataLoaded, value); } }

        private Visibility _VideoVisibility = Visibility.Collapsed;
        public Visibility VideoVisibility { get { return _VideoVisibility; } set { Set(ref _VideoVisibility, value); } }

        private Visibility _ImageVisibility = Visibility.Collapsed;
        public Visibility ImageVisibility { get { return _ImageVisibility; } set { Set(ref _ImageVisibility, value); } }

        private bool _ReplayVideoEnabled = false;
        public bool ReplayVideoEnabled { get { return _ReplayVideoEnabled; } set { Set(ref _ReplayVideoEnabled, value); } }

        private bool _MoreInfoEnabled = false;
        public bool MoreInfoEnabled { get { return _MoreInfoEnabled; } set { Set(ref _MoreInfoEnabled, value); } }

        private int _CurrentQuestionNumberView = 0;
        public int CurrentQuestionNumberView
        {
            get { return _CurrentQuestionNumberView; }
            set { Set(ref _CurrentQuestionNumberView, value); }
        }

        private int _LoadedQuestionsNumber = 0;
        public int LoadedQuestionsNumber
        {
            get { return _LoadedQuestionsNumber; }
            set { Set(ref _LoadedQuestionsNumber, value); }
        }

        private int _CurrentQuestionPoints = 0;
        public int CurrentQuestionPoints
        {
            get { return _CurrentQuestionPoints; }
            set { Set(ref _CurrentQuestionPoints, value); }
        }

        private string _AddToFavouriteValue = "";
        public string AddToFavouriteValue
        {
            get { return _AddToFavouriteValue; }
            set { Set(ref _AddToFavouriteValue, value); base.RaisePropertyChanged(); }
        }

        private string _GotoQuestionValue = "";
        public string GotoQuestionValue
        {
            get { return _GotoQuestionValue; }
            set { Set(ref _GotoQuestionValue, value); base.RaisePropertyChanged(); }
        }

        private Uri _VideoElementSource = null;
        public Uri VideoElementSource
        {
            get { return _VideoElementSource; }
            set { Set(ref _VideoElementSource, value); }
        }

        private BitmapImage _ImageElementSource = null;
        public BitmapImage ImageElementSource
        {
            get { return _ImageElementSource; }
            set { Set(ref _ImageElementSource, value); }
        }

        private string _QuestionText = "Question text";
        public string QuestionText
        {
            get { return _QuestionText; }
            set { Set(ref _QuestionText, value); }
        }

        private bool _FirstAnswerChecked = false;
        public bool FirstAnswerChecked
        {
            get { return _FirstAnswerChecked; }
            set { Set(ref _FirstAnswerChecked, value);  base.RaisePropertyChanged(); }
        }
        private bool _SecondAnswerChecked = false;
        public bool SecondAnswerChecked
        {
            get { return _SecondAnswerChecked; }
            set { Set(ref _SecondAnswerChecked, value); base.RaisePropertyChanged(); }
        }
        private bool _ThirdAnswerChecked = false;
        public bool ThirdAnswerChecked
        {
            get { return _ThirdAnswerChecked; }
            set { Set(ref _ThirdAnswerChecked, value); base.RaisePropertyChanged(); }
        }

        private bool _MoreInfoOpen = false;
        public bool MoreInfoOpen
        {
            get { return _MoreInfoOpen; }
            set { Set(ref _MoreInfoOpen, value); base.RaisePropertyChanged(); }
        }

        private string _FirstAnswerText = "First Option";
        public string FirstAnswerText
        {
            get { return _FirstAnswerText; }
            set { Set(ref _FirstAnswerText, value); }
        }
        private string _SecondAnswerText = "Second Option";
        public string SecondAnswerText
        {
            get { return _SecondAnswerText; }
            set { Set(ref _SecondAnswerText, value); }
        }
        private string _ThirdAnswerText = "Third Option";
        public string ThirdAnswerText
        {
            get { return _ThirdAnswerText; }
            set { Set(ref _ThirdAnswerText, value); }
        }

        private string _QuestionComment = "Question Comment";
        public string QuestionComment
        {
            get { return _QuestionComment; }
            set { Set(ref _QuestionComment, value); }
        }

        private SolidColorBrush _FirstAnswerBackground = null;
        public SolidColorBrush FirstAnswerBackground
        {
            get { return _FirstAnswerBackground; }
            set { Set(ref _FirstAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _SecondAnswerBackground = null;
        public SolidColorBrush SecondAnswerBackground
        {
            get { return _SecondAnswerBackground; }
            set { Set(ref _SecondAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _ThirdAnswerBackground = null;
        public SolidColorBrush ThirdAnswerBackground
        {
            get { return _ThirdAnswerBackground; }
            set { Set(ref _ThirdAnswerBackground, value); base.RaisePropertyChanged(); }
        }

        private Visibility _ThirdAnswerVisibility = Visibility.Visible;
        public Visibility ThirdAnswerVisibility
        {
            get { return _ThirdAnswerVisibility; }
            set { Set(ref _ThirdAnswerVisibility, value); }
        }

        DelegateCommand<bool> _QuestionButtonNextPrevLoadCommand;
        public DelegateCommand<bool> QuestionButtonNextPrevLoadCommand => _QuestionButtonNextPrevLoadCommand ?? (_QuestionButtonNextPrevLoadCommand = new DelegateCommand<bool>(QuestionButtonNextPrevLoader));

        DelegateCommand _IDontKnowCommand;
        public DelegateCommand IDontKnowCommand => _IDontKnowCommand ?? (_IDontKnowCommand = new DelegateCommand(IDontKnow));

        DelegateCommand _MoreInfoQuestionCommand;
        public DelegateCommand MoreInfoQuestionCommand => _MoreInfoQuestionCommand ?? (_MoreInfoQuestionCommand = new DelegateCommand(MoreInfoQuestion));

        DelegateCommand _GotoQuestionCommand;
        public DelegateCommand GotoQuestionCommand => _GotoQuestionCommand ?? (_GotoQuestionCommand = new DelegateCommand(GoToQuestion));

        DelegateCommand<int> _AnswerButtonCheckedCommand;
        public DelegateCommand<int> AnswerButtonCheckedCommand => _AnswerButtonCheckedCommand ?? (_AnswerButtonCheckedCommand = new DelegateCommand<int>(AnswerButtonChecked));

        DelegateCommand _AddToFavouriteCommand;
        public DelegateCommand AddToFavouriteCommand => _AddToFavouriteCommand ?? (_AddToFavouriteCommand = new DelegateCommand(AddToFavouriteAsync));

        int CurrentQuestionId = 0;
        int CurrentQuestionNumber = 0;
        List<Questions> questionsList = new List<Questions>();
        List<Favourites> favouritesList = new List<Favourites>();
    #endregion

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            Views.Busy.SetBusy(true, "Proszę czekać...");
            Category = (suspensionState.ContainsKey(nameof(Category))) ? suspensionState[nameof(Category)]?.ToString() : parameter?.ToString();

            Debug.WriteLine("Uruchomiono z parametrem: " + Category);

            questionsList = await ApiService.Instance.GetQuestionsForCategory(Category);

            Debug.WriteLine("Załadowano pytań: " + questionsList.Count());

            LoadedQuestionsNumber = questionsList.Count();

            favouritesList = await ApiService.Instance.GetUserFavourites(SettingsService.Instance.UserId);

            Debug.WriteLine("Załadowano ulubionych: " + favouritesList.Count());

            StartLearning();

            DataLoaded = Visibility.Visible;
            Views.Busy.SetBusy(false);
        }

        private void LoadImageVideo(int id)
        {
            if (string.IsNullOrEmpty(questionsList[id].Img))
            {
                ImageVisibility = Visibility.Collapsed;
                VideoVisibility = Visibility.Visible;
                ReplayVideoEnabled = true;
                VideoElementSource = new Uri("ms-appx:///Assets/Videos/" + questionsList[id].Video);
            }
            else
            {
                ImageVisibility = Visibility.Visible;
                VideoVisibility = Visibility.Collapsed;
                ReplayVideoEnabled = false;
                ImageElementSource = new BitmapImage(new Uri("ms-appx:///Assets/Images/" + questionsList[id].Img));
            }
        }

        private void LoadPossibleAnswers(int id)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            FirstAnswerChecked = false;
            SecondAnswerChecked = false;
            ThirdAnswerChecked = false;

            if (string.IsNullOrEmpty(questionsList[id].First_answer) && string.IsNullOrEmpty(questionsList[id].Second_answer) &&
                string.IsNullOrEmpty(questionsList[id].Third_answer))
            {
                ThirdAnswerVisibility = Visibility.Collapsed;
                FirstAnswerText = "TAK";
                SecondAnswerText = "NIE";
            }
            else
            {
                ThirdAnswerVisibility = Visibility.Visible;
                FirstAnswerText = questionsList[id].First_answer;
                SecondAnswerText = questionsList[id].Second_answer;
                ThirdAnswerText = questionsList[id].Third_answer;
            }
        }

        private bool CheckIfIsFavourite(int id)
        {
            Favourites favourite = favouritesList.Find(item => item.QuestionId == id);
            if (favourite != null && favourite.IsFavourited)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void ChangeAddToFavouriteButtonText(int id)
        {
            if (CheckIfIsFavourite(id))
            {
                AddToFavouriteValue = "";
            }
            else
            {
                AddToFavouriteValue = "";
            }
        }

        private void MoreInfoCheck(int id)
        {
            if (!string.IsNullOrEmpty(questionsList[id].Comment))
            {
                MoreInfoEnabled = true; 
            }
            else
            {
                MoreInfoEnabled = false;
            }
            QuestionComment = questionsList[id].Comment;
        }

        private void LoadQuestion(int id)
        {
            QuestionText = questionsList[id].Question;
            CurrentQuestionNumberView = (CurrentQuestionNumber + 1);
            CurrentQuestionPoints = questionsList[id].Points;
            CurrentQuestionId = questionsList[id].Id;
            LoadImageVideo(id);
            LoadPossibleAnswers(id);
            ChangeAddToFavouriteButtonText(CurrentQuestionId);
            MoreInfoCheck(id);
        }

        public void StartLearning()
        {
            LoadQuestion(CurrentQuestionNumber);
        }

        private void QuestionButtonNextPrevLoader(bool next)
        {
            if (next)
            {
                if (CurrentQuestionNumber + 1 != questionsList.Count())
                {
                    CurrentQuestionNumber = CurrentQuestionNumber + 1;
                    LoadQuestion(CurrentQuestionNumber);
                }
            }
            else
            {
                if (CurrentQuestionNumber != 0)
                {
                    CurrentQuestionNumber = CurrentQuestionNumber - 1;
                    LoadQuestion(CurrentQuestionNumber);
                }
            }
        }

        private void GoToQuestion()
        {
            if (!string.IsNullOrEmpty(GotoQuestionValue))
            {
                int queryNumber = Int32.Parse(GotoQuestionValue);
                if (queryNumber <= questionsList.Count() + 1)
                {
                    CurrentQuestionNumber = queryNumber - 1;
                    LoadQuestion(CurrentQuestionNumber);
                    GotoQuestionValue = "";
                    //GoToQuestionFlyout.Hide();
                }
            }

        }

        private void AnswerButtonChecked(int sender)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            switch (sender)
            {
                case 1:
                    if (sender == questionsList[CurrentQuestionNumber].Correct_answer) { FirstAnswerBackground = new SolidColorBrush(Colors.Green); } else { FirstAnswerBackground = new SolidColorBrush(Colors.Red); };
                    FirstAnswerChecked = true;
                    break;
                case 2:
                    if (sender == questionsList[CurrentQuestionNumber].Correct_answer) { SecondAnswerBackground = new SolidColorBrush(Colors.Green); } else { SecondAnswerBackground = new SolidColorBrush(Colors.Red); };
                    SecondAnswerChecked = true;
                    break;
                case 3:
                    if (sender == questionsList[CurrentQuestionNumber].Correct_answer) { ThirdAnswerBackground = new SolidColorBrush(Colors.Green); } else { ThirdAnswerBackground = new SolidColorBrush(Colors.Red); };
                    ThirdAnswerChecked = true;
                    break;
            }
        }

        private void IDontKnow()
        {
            AnswerButtonCheckedCommand.Execute(questionsList[CurrentQuestionNumber].Correct_answer);
        }

        private void MoreInfoQuestion()
        {
            MoreInfoOpen = !MoreInfoOpen;
        }

        private async void AddToFavouriteAsync()
        {
            Favourites item = new Favourites()
            {
                UserId = SettingsService.Instance.UserId,
                QuestionId = CurrentQuestionId
            };

            if (!CheckIfIsFavourite(CurrentQuestionId))
            {
                item.IsFavourited = true;
                if (await ApiService.Instance.AddUserFavourite(item))
                {
                    favouritesList.Add(item);
                    ChangeAddToFavouriteButtonText(CurrentQuestionId);
                }
            }
            else
            {
                item.IsFavourited = false;
                if(await ApiService.Instance.DeleteUserFavourite(item))
                {
                    Favourites index = favouritesList.Find(w => w.QuestionId == item.QuestionId);
                    favouritesList.Remove(index);
                    ChangeAddToFavouriteButtonText(CurrentQuestionId);
                }
            }
        }
    }
}
