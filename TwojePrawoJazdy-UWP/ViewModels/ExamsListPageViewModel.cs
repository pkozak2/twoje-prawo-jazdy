﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    class ExamsListPageViewModel : ViewModelBase
    {
        #region Definitions
        private ObservableCollection<ExamsListPageItem> _TestsCollection = new ObservableCollection<ExamsListPageItem>();
        public ObservableCollection<ExamsListPageItem> TestsCollection
        {
            get { return _TestsCollection; }
            set { Set(ref _TestsCollection, value); }
        }

        private ExamsListPageItem _TestCollectionSelectedItem;
        public ExamsListPageItem TestCollectionSelectedItem
        {
            get { return _TestCollectionSelectedItem; }
            set { Set(ref _TestCollectionSelectedItem, value);
                LoadExamDetails();
            }
        }

        private List<Tests> TestsList = new List<Tests>();

        private Visibility _EmptyListVisibility = Visibility.Visible;
        public Visibility EmptyListVisibility
        {
            get { return _EmptyListVisibility; }
            set { Set(ref _EmptyListVisibility, value); }
        }

        private float _AverageTotal = 0;
        public float AverageTotal
        {
            get { return _AverageTotal; }
            set { Set(ref _AverageTotal, value); }
        }

        private int _CategoryACount = 0;
        public int CategoryACount
        {
            get { return _CategoryACount; }
            set { Set(ref _CategoryACount, value); }
        }

        private int _CategoryBCount = 0;
        public int CategoryBCount
        {
            get { return _CategoryBCount; }
            set { Set(ref _CategoryBCount, value); }
        }

        private int _CategoryCCount = 0;
        public int CategoryCCount
        {
            get { return _CategoryCCount; }
            set { Set(ref _CategoryCCount, value); }
        }

        private int _CategoryDCount = 0;
        public int CategoryDCount
        {
            get { return _CategoryDCount; }
            set { Set(ref _CategoryDCount, value); }
        }

        private int _CategoryTCount = 0;
        public int CategoryTCount
        {
            get { return _CategoryTCount; }
            set { Set(ref _CategoryTCount, value); }
        }
        #endregion

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            Views.Busy.SetBusy(true, "Trwa ładowanie danych...");

            int counter = 1;

            TestsList = await ApiService.Instance.GetUserExams(SettingsService.Instance.UserId);
            if (TestsList.Count > 0)
            {
                EmptyListVisibility = Visibility.Collapsed;
            }

            foreach(Tests test in TestsList)
            {
                SolidColorBrush resultColor = new SolidColorBrush(Colors.DarkRed);
                string Result = "NEGATYWNY";
                if (test.IsPassed)
                {
                    Result = "POZYTYWNY";
                    resultColor = new SolidColorBrush(Colors.DarkGreen);
                }
                TestsCollection.Add(new ExamsListPageItem {TestId = test.TestId,
                                    Number = counter++,
                                    Result = Result,
                                    Category = test.Category,
                                    Points = test.Points,
                                    SaveDate = test.SaveDate,
                                    ResultColor = resultColor
                                    });
            }

            CountExams();



            Views.Busy.SetBusy(false);
            await Task.CompletedTask;
        }

        private void LoadExamDetails()
        {
            Tests test = TestsList.Where(i => i.TestId == TestCollectionSelectedItem.TestId).First();
            var state = Template10.Common.BootStrapper.Current.SessionState;
            state.Add("Results", test);

            NavigationService.Navigate(typeof(Views.ExamResultPage), "Results");
        }

        private void CountExams()
        {
            CategoryACount = TestsList.Count(id => id.Category.ToUpper().Contains("A"));
            CategoryBCount = TestsList.Count(id => id.Category.ToUpper().Contains("B"));
            CategoryCCount = TestsList.Count(id => id.Category.ToUpper().Contains("C"));
            CategoryDCount = TestsList.Count(id => id.Category.ToUpper().Contains("D"));
            CategoryTCount = TestsList.Count(id => id.Category.ToUpper().Contains("T"));

            float passedCount = TestsList.Count(id => id.IsPassed == true);
            float tests = TestsCollection.Count();
            AverageTotal = (passedCount / tests) * 100;
        }
    }
}
