using Template10.Mvvm;
using System.Collections.Generic;
using System;
using System.Linq;
using System.Threading.Tasks;
using Template10.Services.NavigationService;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        DelegateCommand<string> _GotoLearn;
        DelegateCommand<string> _GotoExam;

        public DelegateCommand<string> GotoLearn => _GotoLearn ?? (_GotoLearn = new DelegateCommand<string>(LoadLearnPage));
        public DelegateCommand<string> GotoExam => _GotoExam ?? (_GotoExam = new DelegateCommand<string>(LoadExamPage));

        void LoadLearnPage(string category)
        { 
            if (category == null)
            {
                throw new NullReferenceException("category is not set");
            }
            else
            {
                 NavigationService.Navigate(typeof(Views.LearnPage), category);
            }
        }

        void LoadExamPage(string category)
        {
            if (category == null)
            {
                throw new NullReferenceException("category is not set");
            }
            else
            {
                NavigationService.Navigate(typeof(Views.ExamPage), category);
            }
        }

        public void GotoSettings() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 0);

        public void GotoPremium() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 1);

        public void GotoAbout() =>
            NavigationService.Navigate(typeof(Views.SettingsPage), 2);
    }
}

