﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Template10.Mvvm;
using TwojePrawoJazdy_UWP.Models;
using TwojePrawoJazdy_UWP.Services.ApiServices;
using TwojePrawoJazdy_UWP.Services.SettingsServices;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace TwojePrawoJazdy_UWP.ViewModels
{
    class ExamResultPageViewModel : ViewModelBase
    { 
        List<Questions> questionsList = new List<Questions>();
        private int resultPoints;
        private int selectedQuestion = 0;
        private bool isPassed = false;

        private string _ResultText = "NEGATYWNY";
        public string ResultText
        {
            get { return _ResultText; }
            set { Set(ref _ResultText, value); }
        }

        private int _ResultPointsText = 0;
        public int ResultPointsText
        {
            get { return _ResultPointsText; }
            set { Set(ref _ResultPointsText, value); }
        }

        private Uri _VideoElementSource = null;
        public Uri VideoElementSource
        {
            get { return _VideoElementSource; }
            set { Set(ref _VideoElementSource, value); }
        }

        private BitmapImage _ImageElementSource = null;
        public BitmapImage ImageElementSource
        {
            get { return _ImageElementSource; }
            set { Set(ref _ImageElementSource, value); }
        }

        private string _QuestionText = "Question text";
        public string QuestionText
        {
            get { return _QuestionText; }
            set { Set(ref _QuestionText, value); }
        }

        private bool _FirstAnswerChecked = false;
        public bool FirstAnswerChecked
        {
            get { return _FirstAnswerChecked; }
            set { Set(ref _FirstAnswerChecked, value); base.RaisePropertyChanged(); }
        }
        private bool _SecondAnswerChecked = false;
        public bool SecondAnswerChecked
        {
            get { return _SecondAnswerChecked; }
            set { Set(ref _SecondAnswerChecked, value); base.RaisePropertyChanged(); }
        }
        private bool _ThirdAnswerChecked = false;
        public bool ThirdAnswerChecked
        {
            get { return _ThirdAnswerChecked; }
            set { Set(ref _ThirdAnswerChecked, value); base.RaisePropertyChanged(); }
        }

        private bool _IsAutoSaveEnabled = !SettingsService.Instance.AutoSaveExams;
        public bool IsAutoSaveEnabled
        {
            get { return _IsAutoSaveEnabled; }
            set { Set(ref _IsAutoSaveEnabled, value);}
        }

        private string _FirstAnswerText = "First Option";
        public string FirstAnswerText
        {
            get { return _FirstAnswerText; }
            set { Set(ref _FirstAnswerText, value); }
        }
        private string _SecondAnswerText = "Second Option";
        public string SecondAnswerText
        {
            get { return _SecondAnswerText; }
            set { Set(ref _SecondAnswerText, value); }
        }
        private string _ThirdAnswerText = "Third Option";
        public string ThirdAnswerText
        {
            get { return _ThirdAnswerText; }
            set { Set(ref _ThirdAnswerText, value); }
        }

        private SolidColorBrush _FirstAnswerBackground = null;
        public SolidColorBrush FirstAnswerBackground
        {
            get { return _FirstAnswerBackground; }
            set { Set(ref _FirstAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _SecondAnswerBackground = null;
        public SolidColorBrush SecondAnswerBackground
        {
            get { return _SecondAnswerBackground; }
            set { Set(ref _SecondAnswerBackground, value); base.RaisePropertyChanged(); }
        }
        private SolidColorBrush _ThirdAnswerBackground = null;
        public SolidColorBrush ThirdAnswerBackground
        {
            get { return _ThirdAnswerBackground; }
            set { Set(ref _ThirdAnswerBackground, value); base.RaisePropertyChanged(); }
        }

        private Visibility _VideoVisibility = Visibility.Collapsed;
        public Visibility VideoVisibility { get { return _VideoVisibility; } set { Set(ref _VideoVisibility, value); } }

        private Visibility _ImageVisibility = Visibility.Collapsed;
        public Visibility ImageVisibility { get { return _ImageVisibility; } set { Set(ref _ImageVisibility, value); } }

        private Visibility _ThirdAnswerVisibility = Visibility.Visible;
        public Visibility ThirdAnswerVisibility
        {
            get { return _ThirdAnswerVisibility; }
            set { Set(ref _ThirdAnswerVisibility, value); }
        }

        private bool _PopupVisibility = false;
        public bool PopupVisibility
        {
            get { return _PopupVisibility; }
            set { Set(ref _PopupVisibility, value); }
        }

        private SolidColorBrush _ResultTextForeground = new SolidColorBrush(Colors.DarkRed);
        public SolidColorBrush ResultTextForeground
        {
            get { return _ResultTextForeground; }
            set { Set(ref _ResultTextForeground, value); }
        }

        public EventHandler<Questions> AddButtonEvent;

        DelegateCommand _ClosePopupCommand;
        public DelegateCommand ClosePopupCommand => _ClosePopupCommand ?? (_ClosePopupCommand = new DelegateCommand(ClosePopup));

        DelegateCommand<int> _AnswerButtonCheckedCommand;
        public DelegateCommand<int> AnswerButtonCheckedCommand => _AnswerButtonCheckedCommand ?? (_AnswerButtonCheckedCommand = new DelegateCommand<int>(AnswerButtonChecked));

        DelegateCommand<int> _ShowDetailsCommand;
        public DelegateCommand<int> ShowDetailsCommand => _ShowDetailsCommand ?? (_ShowDetailsCommand = new DelegateCommand<int>(LoadQuestionPopup));

        DelegateCommand _SaveExamCommand;
        public DelegateCommand SaveExamCommand => _SaveExamCommand ?? (_SaveExamCommand = new DelegateCommand(SaveExamAsync));

        public override async Task OnNavigatedToAsync(object parameter, NavigationMode mode, IDictionary<string, object> suspensionState)
        {
            Views.Busy.SetBusy(true, "Trwa ładowanie danych...");
            var param = parameter.ToString();
            if (SessionState.ContainsKey(param) & param == "Questions")
            {
                var value = SessionState[param] as List<Questions>;
                questionsList = value;
                SessionState.Remove("Questions");
                CountPoints();

                if (SettingsService.Instance.AutoSaveExams && SaveExamCommand.CanExecute())
                {
                    SaveExamCommand.Execute();
                }
            }
            else if (SessionState.ContainsKey(param) & param == "Results")
            {
                var value = SessionState[param] as Tests;
                await LoadSavedExamResultAsync(value);
                SessionState.Remove("Results");
            }

            int NumOfButtons = questionsList.Count();


            for (int i = 1; i <= NumOfButtons; i++)
            {
                AddButtonEvent(this, questionsList[i - 1]);
            }

            Views.Busy.SetBusy(false);
            await Task.CompletedTask;
        }

        private async Task LoadSavedExamResultAsync(Tests test)
        {
            IsAutoSaveEnabled = false;
            ResultPointsText = test.Points;
            if (test.Points >= 68)
            {
                ResultTextForeground = new SolidColorBrush(Colors.DarkGreen);
                ResultText = "POZYTYWNY";
            }
            List<int> ids = test.TestsQuestions.Select(o => o.QuestionId).ToList();
            questionsList = await ApiService.Instance.GetQuestionsByIds(ids);

            foreach (TestsQuestions element in test.TestsQuestions)
            {
                questionsList.Where(i => i.Id == element.QuestionId).First().TempSelectedAnswer = element.SelectedAnswer;
            }
        }

        private void CountPoints()
        {
            foreach (Questions question in questionsList)
            {
                if (question.Correct_answer == question.TempSelectedAnswer)
                {
                    resultPoints += question.Points;
                }
            }
            if (resultPoints >= 68)
            {
                ResultTextForeground = new SolidColorBrush(Colors.DarkGreen);
                ResultText = "POZYTYWNY";
                isPassed = true;
            }
            ResultPointsText = resultPoints;
        }

        private void LoadQuestionPopup(int obj)
        {
            PopupVisibility = true;
            selectedQuestion = obj;

            LoadPossibleAnswers(selectedQuestion);
            LoadImageVideo(selectedQuestion);

            AnswerButtonCheckedCommand.Execute(questionsList[selectedQuestion].TempSelectedAnswer);

            QuestionText = questionsList[selectedQuestion].Question;
        }

        private void ClosePopup()
        {
            PopupVisibility = false;
        }

        private void LoadPossibleAnswers(int id)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            FirstAnswerChecked = false;
            SecondAnswerChecked = false;
            ThirdAnswerChecked = false;

            if (string.IsNullOrEmpty(questionsList[id].First_answer) && string.IsNullOrEmpty(questionsList[id].Second_answer) &&
                string.IsNullOrEmpty(questionsList[id].Third_answer))
            {
                ThirdAnswerVisibility = Visibility.Collapsed;
                FirstAnswerText = "TAK";
                SecondAnswerText = "NIE";
            }
            else
            {
                ThirdAnswerVisibility = Visibility.Visible;
                FirstAnswerText = questionsList[id].First_answer;
                SecondAnswerText = questionsList[id].Second_answer;
                ThirdAnswerText = questionsList[id].Third_answer;
            }
        }

        private void LoadImageVideo(int id)
        {
            if (string.IsNullOrEmpty(questionsList[id].Img))
            {
                ImageVisibility = Visibility.Collapsed;
                VideoVisibility = Visibility.Visible;
                VideoElementSource = new Uri("ms-appx:///Assets/Videos/" + questionsList[id].Video);
            }
            else
            {
                ImageVisibility = Visibility.Visible;
                VideoVisibility = Visibility.Collapsed;
                ImageElementSource = new BitmapImage(new Uri("ms-appx:///Assets/Images/" + questionsList[id].Img));
            }
        }

        private void AnswerButtonChecked(int sender)
        {
            FirstAnswerBackground = new SolidColorBrush(Colors.DimGray);
            SecondAnswerBackground = new SolidColorBrush(Colors.DimGray);
            ThirdAnswerBackground = new SolidColorBrush(Colors.DimGray);
            switch (sender)
            {
                case 1:
                    if (sender == questionsList[selectedQuestion].Correct_answer) { FirstAnswerBackground = new SolidColorBrush(Colors.Green); } else { FirstAnswerBackground = new SolidColorBrush(Colors.Red); };
                    FirstAnswerChecked = true;
                    break;
                case 2:
                    if (sender == questionsList[selectedQuestion].Correct_answer) { SecondAnswerBackground = new SolidColorBrush(Colors.Green); } else { SecondAnswerBackground = new SolidColorBrush(Colors.Red); };
                    SecondAnswerChecked = true;
                    break;
                case 3:
                    if (sender == questionsList[selectedQuestion].Correct_answer) { ThirdAnswerBackground = new SolidColorBrush(Colors.Green); } else { ThirdAnswerBackground = new SolidColorBrush(Colors.Red); };
                    ThirdAnswerChecked = true;
                    break;
            }
        }


        private async void SaveExamAsync()
        {
            Views.Busy.SetBusy(true, "Proszę czekać. Trwa zapis wyniku.");
            await Task.Run(async () =>
            {
                Tests exam = new Tests()
                {
                    UserId = SettingsService.Instance.UserId,
                    Category = questionsList[0].Categories,
                    Points = resultPoints,
                    IsPassed = isPassed
                };
                foreach (Questions question in questionsList)
                {
                    exam.TestsQuestions.Add(new TestsQuestions() { QuestionId = question.Id, SelectedAnswer = question.TempSelectedAnswer });
                }

                bool result = await ApiService.Instance.SaveUserExam(exam);
                if (result)
                {
                    IsAutoSaveEnabled = false;
                }                
            });
           
            Views.Busy.SetBusy(false);

        }

    }
}
