﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwojePrawoJazdy_WebApi.Models;

namespace TwojePrawoJazdy_WebApi.Controllers
{
    public class TestsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Pobiera wszystkie zapisane testy. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <returns>Lista testów</returns>
        // GET: Tests
        //[Authorize(Roles = "Admin")]
        public List<Tests> GetTests()
        {
            var tests = db.Tests.ToList();
            return tests;
        }

        /// <summary>
        /// Pobiera wszystkie zapisane testy dla danego użytkownika. Wymagana autoryzacja.
        /// </summary>
        /// <param name="userId">ID Użytkownika</param>
        /// <returns>Lista testów</returns>
        //[Authorize]
        [Route("Tests/user/{userId}")]
        public List<Tests> GetTests(string userId)
        {
            var tests = db.Tests.Where(w => w.UserId == userId).OrderBy(x => x.SaveDate).ToList();

            return tests;
        }

        // GET: Tests/5
        /// <summary>
        /// Pobiera test o wybranym ID. Wymagana autoryzacja.
        /// </summary>
        /// <param name="id">ID testu</param>
        /// <returns>Test wraz z listą ID pytań oraz odpowiedzi.</returns>
        //[Authorize]
        [ResponseType(typeof(Tests))]
        public async Task<IHttpActionResult> GetTests(int id)
        {
            Tests tests = await db.Tests.FindAsync(id);
            if (tests == null)
            {
                return NotFound();
            }

            return Ok(tests);
        }

        // PUT: Tests/5
        /// <summary>
        /// Aktualizuje wybrany test. Wymagana autoryzacja.
        /// </summary>
        /// <param name="id">ID testu</param>
        /// <param name="tests">Zaktualizowany test.</param>
        /// <returns></returns>
        //[Authorize]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTests(int id, Tests tests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tests.TestId)
            {
                return BadRequest();
            }

            db.Entry(tests).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TestsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: Tests
        /// <summary>
        /// Dodaje ukończony test do bazy danych. Wymagana autoryzacja.
        /// </summary>
        /// <param name="tests">Pola z klasy Test</param>
        /// <returns></returns>
        //[Authorize]
        [ResponseType(typeof(Tests))]
        public async Task<IHttpActionResult> PostTests(Tests tests)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            tests.SaveDate = DateTime.Now;

            db.Tests.Add(tests);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tests.TestId }, tests);
        }

        // DELETE: Tests/5
        /// <summary>
        /// Usunięcie testu z bazy danych. Wymagana autoryzacja.
        /// </summary>
        /// <param name="id">ID testu</param>
        /// <returns></returns>
        //[Authorize]
        [ResponseType(typeof(Tests))]
        public async Task<IHttpActionResult> DeleteTests(int id)
        {
            Tests tests = await db.Tests.FindAsync(id);
            if (tests == null)
            {
                return NotFound();
            }

            db.Tests.Remove(tests);
            await db.SaveChangesAsync();

            return Ok(tests);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TestsExists(int id)
        {
            return db.Tests.Count(e => e.TestId == id) > 0;
        }
    }
}