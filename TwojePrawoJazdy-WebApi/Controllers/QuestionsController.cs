﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwojePrawoJazdy_WebApi.Helpers;
using TwojePrawoJazdy_WebApi.Models;

namespace TwojePrawoJazdy_WebApi.Controllers
{
   // [Authorize]
   /// <summary>
   /// Kontroler klasy Question, pozwalający na dostęp do danych w tabeli przechowującej pytania egzaminacyjne.
   /// </summary>
    public class QuestionsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        /// <summary>
        /// Pobiera wszystkie pytania z bazy. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <returns>Lista pytań</returns>
        // GET: Questions
        [Authorize(Roles="Admin")]
        public IQueryable<Question> GetQuestions()
        {
            return db.Questions;
        }

        // GET: Questions?category=abcdt
        /// <summary>
        /// Pobiera wszystkie pytania dla danej kategorii. Wymagana autoryzacja.
        /// </summary>
        /// <param name="category">Oznaczenie kategorii (np. a)</param>
        /// <returns>Lista pytań dla kategorii</returns>
        [Authorize]
        [Route("Questions/{category}")]
        public IQueryable<Question> GetQuestionsForCategory(string category)
        {
            return db.Questions.Where(w => w.categories.Contains(category));
        }

        /// <summary>
        /// Pobiera listę ID pytań zależnie od parametru premium oraz kategorii. Wymagana autoryzacja.
        /// </summary>
        /// <param name="category">Kategoria pytań</param>
        /// <param name="premium">Czy użytkownik ma uprawnienia do rozszerzonej bazy pytań</param>
        /// <returns></returns>
        [Authorize]
        [Route("Questions/{category}/{premium}/ids")]
        public List<int> GetQuestionsIds(string category, bool premium)
        {
            var questions = GetQuestionsForCategoryAndPremium(category, premium);
            List<int> ids = new List<int>();

            foreach(Question q in questions)
            {
                ids.Add(q.id);
            }
            return ids;
        }
        /// <summary>
        /// Pobiera listę pytań zależnie od parametru premium oraz kategorii. Wymagana autoryzacja.
        /// </summary>
        /// <param name="category">Kategoria pytań</param>
        /// <param name="premium">Czy użytkownik ma uprawnienia do rozszerzonej bazy pytań</param>
        /// <returns></returns>
        // GET: Questions?category=a&premium=false
        [Authorize]
        [Route("Questions/{category}/{premium}")]
        public List<Question> GetQuestionsForCategoryAndPremium(string category, bool premium)
        {
           
            if (premium)
            {
                return db.Questions.Where(w => w.categories.Contains(category)).ToList();
            }
            
            return db.Questions.Where(w => w.categories.Contains(category) && w.is_premium == 0).ToList();

        }
        /// <summary>
        /// Losuje wybraną liczbę pytań zależnie od wybranych parametrów. Wymagana autoryzacja.
        /// </summary>
        /// <param name="category">Kategoria z której losowane są pytania</param>
        /// <param name="premium">Czy użytkownik ma uprawnienia do rozszerzonej bazy pytań</param>
        /// <param name="type">Typ pytania (podstawowe, specjalistyczne)</param>
        /// <param name="points">Ilość punktów</param>
        /// <param name="count">Liczba losowanych pytań</param>
        /// <returns></returns>
        // GET: Questions?category=a&premium=false&type=basic&points=1&count=2
        [Authorize]
        [Route("Questions/{category}/{premium}/{type}/{points}/{count}")]
        public IQueryable<Question> GetQuestionsForCategoryExam(string category, bool premium, string type, int points, int count)
        {
            var rand = new Random();

            if (premium)
            {
                return db.Questions.Where(w => w.categories.Contains(category) && w.typ == type && w.points == points).OrderBy(t => Guid.NewGuid()).Take(count);
            }
            return db.Questions.Where(w => w.categories.Contains(category) && w.typ == type && w.points == points && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(count);

        }

        /// <summary>
        /// Losuje egzamin dla danej kategorii. Wymagana autoryzacja.
        /// </summary>
        /// <param name="category">Kategoria z której losowane są pytania</param>
        /// <param name="premium">Czy użytkownik ma uprawnienia do rozszerzonej bazy pytań</param>
        /// <returns></returns>
        [Authorize]
        [Route("Questions/{category}/{premium}/exam")]
        public List<Question> GetExamForCategory(string category, bool premium)
        {
            List<Question> examQuestions = new List<Question>();
            var rand = new Random();

            if (premium)
            {

                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 3).OrderBy(t => Guid.NewGuid()).Take(10).ToList());
                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 2).OrderBy(t => Guid.NewGuid()).Take(6).ToList());
                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 1).OrderBy(t => Guid.NewGuid()).Take(4).ToList());

                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 3).OrderBy(t => Guid.NewGuid()).Take(6).ToList());
                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 2).OrderBy(t => Guid.NewGuid()).Take(4).ToList());
                examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 1).OrderBy(t => Guid.NewGuid()).Take(2).ToList());

                examQuestions.Shuffle();
                return examQuestions;
            }
            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 3 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(10).ToList());
            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 2 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(6).ToList());
            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "basic" && w.points == 1 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(4).ToList());

            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 3 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(6).ToList());
            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 2 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(4).ToList());
            examQuestions.AddRange(db.Questions.Where(w => w.categories.Contains(category) && w.typ == "specialized" && w.points == 1 && w.is_premium == 0).OrderBy(t => Guid.NewGuid()).Take(2).ToList());

            examQuestions.Shuffle();
            return examQuestions;
        }

        /// <summary>
        /// Pobiera wybrane pytanie. Wymagana autoryzacja.
        /// </summary>
        /// <param name="id">ID pytania</param>
        /// <returns></returns>
        // GET: Questions/5
        [Authorize]
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> GetQuestion(int id)
        {
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }

            return Ok(question);
        }
        /// <summary>
        /// Edytuje wybrane pytanie. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <param name="id">ID pytania</param>
        /// <param name="question">Pola z klasy Question</param>
        /// <returns></returns>
        // PUT: Questions/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutQuestion(int id, Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != question.id)
            {
                return BadRequest();
            }

            db.Entry(question).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!QuestionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }
        /// <summary>
        /// Pobiera pytania dla listy ID pytań. Wymagana autoryzacja.
        /// </summary>
        /// <param name="ids">Lista ID pytań</param>
        /// <returns>Lista pytań o podanych ID</returns>
        // GET: Questions/
        [Authorize]
        [Route("Questions/Ids")]
        [ResponseType(typeof(List<Question>))]
        public List<Question> PostQuestionByIds(List<int> ids)
        {
            List<Question> question = db.Questions.Where(x => ids.Contains(x.id)).ToList();

            return question;
        }
        /// <summary>
        /// Dodaje nowe pytanie. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <param name="question">Wszystkie pola z klasy Question</param>
        /// <returns></returns>
        // POST: Questions
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> PostQuestion(Question question)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Questions.Add(question);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = question.id }, question);
        }
        /// <summary>
        /// Kasuje wybrane pytanie. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE: Questions/5
        [Authorize(Roles = "Admin")]
        [ResponseType(typeof(Question))]
        public async Task<IHttpActionResult> DeleteQuestion(int id)
        {
            Question question = await db.Questions.FindAsync(id);
            if (question == null)
            {
                return NotFound();
            }

            db.Questions.Remove(question);
            await db.SaveChangesAsync();

            return Ok(question);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool QuestionExists(int id)
        {
            return db.Questions.Count(e => e.id == id) > 0;
        }
    }
}