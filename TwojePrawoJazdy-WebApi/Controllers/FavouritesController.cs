﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TwojePrawoJazdy_WebApi.Models;

namespace TwojePrawoJazdy_WebApi.Controllers
{
    public class FavouritesController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        /// <summary>
        /// Pobiera wszystkie dostępne rekordy ulubionych pytań wszystkich użytkowników. Wymagane logowanie na koncie Administracyjnym.
        /// </summary>
        /// <returns>Lista wszystkich ulubionych pytań.</returns>
        // GET: Favourites
        [Authorize(Roles = "Admin")]
        public IQueryable<UsersFavourites> GetUsersFavourites()
        {
            return db.UsersFavourites;
        }
        /// <summary>
        /// Pobiera listę ulubionych pytań danego użytkownika. Wymagana autoryzacja.
        /// </summary>
        /// <param name="userId">ID użytkownika</param>
        /// <returns>Lista ulubionych pytań danego użytkownika</returns>
        [Authorize]
        [Route("Favourites/user/{userId}")]
        public List<UsersFavourites> GetUsersFavourites(string userId)
        {
            var favourites = db.UsersFavourites.Where(w => w.UserId == userId).ToList();

            return favourites;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="usersFavourites"></param>
        ///// <returns></returns>
        //// PUT: api/UsersFavourites/5
        //[Authorize]
        //[ResponseType(typeof(void))]
        //public async Task<IHttpActionResult> PutUsersFavourites(string id, UsersFavourites usersFavourites)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    if (id != usersFavourites.UserId)
        //    {
        //        return BadRequest();
        //    }

        //    db.Entry(usersFavourites).State = EntityState.Modified;

        //    try
        //    {
        //        await db.SaveChangesAsync();
        //    }
        //    catch (DbUpdateConcurrencyException)
        //    {
        //        if (!UsersFavouritesExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
        //        {
        //            throw;
        //        }
        //    }

        //    return StatusCode(HttpStatusCode.NoContent);
        //}
        /// <summary>
        /// Dodaje nowe pytanie ulubione do bazy danych. Wymagana autoryzacja.
        /// </summary>
        /// <param name="usersFavourites">Dodawany rekord do bazy danych</param>
        /// <returns>Kod HTTP</returns>
        // POST: api/UsersFavourites
        [Authorize]
        [ResponseType(typeof(UsersFavourites))]
        public async Task<IHttpActionResult> PostUsersFavourites(UsersFavourites usersFavourites)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UsersFavourites.Add(usersFavourites);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (UsersFavouritesExists(usersFavourites.UserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = usersFavourites.UserId }, usersFavourites);
        }

        /// <summary>
        /// Kasuje rekord z ulubionym pytaniem z bazy danych. Wymagana autoryzacja.
        /// </summary>
        /// <param name="userId">ID użytkownika</param>
        /// <param name="questionId">ID pytania</param>
        /// <returns>Usunięty obiekt</returns>
        // DELETE: api/UsersFavourites/userId/questionId
        [Authorize]
        [Route("Favourites/{userId}/{questionId}")]
        [ResponseType(typeof(UsersFavourites))]
        public async Task<IHttpActionResult> DeleteUsersFavourites(string userId, int questionId)
        {
            UsersFavourites usersFavourites = await db.UsersFavourites.FindAsync(userId, questionId);
            if (usersFavourites == null)
            {
                return NotFound();
            }

            db.UsersFavourites.Remove(usersFavourites);
            await db.SaveChangesAsync();

            return Ok(usersFavourites);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsersFavouritesExists(string id)
        {
            return db.UsersFavourites.Count(e => e.UserId == id) > 0;
        }
    }
}