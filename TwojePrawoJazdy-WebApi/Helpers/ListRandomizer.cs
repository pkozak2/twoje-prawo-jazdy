﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace TwojePrawoJazdy_WebApi.Helpers
{
    /// <summary>
    /// Klasa pozwalająca na przemieszanie elementów na liście.
    /// </summary>
    public static class ListRandomizer
    {
        /// <summary>
        /// Metoda sortująca listę danego typu w sposób losowy
        /// </summary>
        /// <typeparam name="T">Kasa listy</typeparam>
        /// <param name="list">Lista elementów danej klasy</param>
        public static void Shuffle<T>(this IList<T> list)
        {
            RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();
            int n = list.Count;
            while (n > 1)
            {
                byte[] box = new byte[1];
                do provider.GetBytes(box);
                while (!(box[0] < n * (Byte.MaxValue / n)));
                int k = (box[0] % n);
                n--;
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
    }
}