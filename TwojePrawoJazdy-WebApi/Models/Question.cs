﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwojePrawoJazdy_WebApi.Models
{
    /// <summary>
    /// Klasa tabeli przechowującej pytania testowe
    /// </summary>
    public class Question
    {
        /// <summary>
        /// Id pytania
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }
        /// <summary>
        /// Treść pytania
        /// </summary>
        public string question { get; set; }
        /// <summary>
        /// Pierwsza możliwa odpowiedź
        /// </summary>
        public string first_answer { get; set; }
        /// <summary>
        /// Druga możliwa odpowiedź
        /// </summary>
        public string second_answer { get; set; }
        /// <summary>
        /// Trzecia możliwa odpowiedź
        /// </summary>
        public string third_answer { get; set; }
        /// <summary>
        /// Nazwa pliku ze zdjęciem do pytania
        /// </summary>
        public string img { get; set; }
        /// <summary>
        /// Nazwa pliku z filmem do pytania
        /// </summary>
        public string video { get; set; }
        /// <summary>
        /// Punkty które można uzyskać za pytanie
        /// </summary>
        public int points { get; set; }
        /// <summary>
        /// Poprawna odpowiedź
        /// </summary>
        public string correct_answer { get; set; }
        /// <summary>
        /// Typ pytania, np. podstawowe, specjalistyczne
        /// </summary>
        public string typ { get; set; }
        /// <summary>
        /// Komentarz do pytania - opis z kodeksu drogowego
        /// </summary>
        public string comment { get; set; }
        /// <summary>
        /// Kategorie do których należy pytanie
        /// </summary>
        public string categories { get; set; }
        /// <summary>
        /// Status pytania do wersji premium aplikacji
        /// </summary>
        public int is_premium { get; set; }

        /// <summary>
        /// Relacja do tabeli łączącej pytania z testami
        /// </summary>
        [JsonIgnore]
        public virtual ICollection<TestsQuestions> TestsQuestions { get; set; }

    }
}