﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwojePrawoJazdy_WebApi.Models
{
    /// <summary>
    /// Klasa przechowująca testy użytkowników wraz z kategorią testu, punktami i informacją, czy test został zdany
    /// </summary>
    public class Tests
    {
        /// <summary>
        /// Id testu
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int TestId { get; set; }
        /// <summary>
        /// Id użytkownika, który wykonał test
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// Kategoria wykonanego testu
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// Punkty, które zostały zdobyte
        /// </summary>
        public int Points { get; set; }
        /// <summary>
        /// Informacja, czy test został zaliczony, czy nie
        /// </summary>
        public bool IsPassed { get; set; }

        /// <summary>
        /// Data zapisu testu do bazy
        /// </summary>
        public DateTime SaveDate { get; set; }

        /// <summary>
        /// Relacja do tabeli użytkowników
        /// </summary>
        [ForeignKey("UserId")]
        [JsonIgnore]
        public virtual ApplicationUser ApplicationUser { get; set; }
        /// <summary>
        /// Relacja do tabeli łączącej pytania z testami
        /// </summary>
        public virtual ICollection<TestsQuestions> TestsQuestions { get; set; }
    }
}