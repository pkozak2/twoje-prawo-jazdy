﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwojePrawoJazdy_WebApi.Models
{
    /// <summary>
    /// Klasa tabeli łączącej ulubione pytania z użytkownikami
    /// </summary>
    public class UsersFavourites
    {
        /// <summary>
        /// Id użytkownika
        /// </summary>
        [Key, Column(Order = 0)]
        public string UserId { get; set; }
        /// <summary>
        /// Id Pytania
        /// </summary>
        [Key, Column(Order = 1)]
        public int QuestionId { get; set; }
        /// <summary>
        /// Flaga określająca, że pytanie ma status Ulubione
        /// </summary>
        public bool IsFavourited { get; set; }

        /// <summary>
        /// Relacja do tabeli użytkowników
        /// </summary>
        [ForeignKey("UserId")]
        [JsonIgnore]
        public virtual ApplicationUser ApplicationUser { get; set; }
        /// <summary>
        /// Relacja do tabeli pytań
        /// </summary>
        [JsonIgnore]
        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }
    }
}