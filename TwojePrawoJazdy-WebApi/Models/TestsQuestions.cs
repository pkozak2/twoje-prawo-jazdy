﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace TwojePrawoJazdy_WebApi.Models
{
    /// <summary>
    /// Klasa tabeli łączącej testy z pytaniami
    /// </summary>
    public class TestsQuestions
    {
        /// <summary>
        /// Id Pytania
        /// </summary>
        [Key, Column(Order = 0)]
        public int QuestionId { get; set; }
        /// <summary>
        /// Id testu
        /// </summary>
        [JsonIgnore]
        [Key, Column(Order = 1)]
        public int TestId { get; set; }
        /// <summary>
        /// Wybrana odpowiedź przez użytkownika
        /// </summary>
        public int SelectedAnswer { get; set; }

        /// <summary>
        /// Relacja do tabeli pytań
        /// </summary>
        [JsonIgnore]
        [ForeignKey("QuestionId")]
        public virtual Question Question { get; set; }
        /// <summary>
        /// Relacja do tabeli testów
        /// </summary>
        [JsonIgnore]
        [ForeignKey("TestId")]
        public virtual Tests Tests { get; set; }
    }
}